//
//  NSString+Utils.h
//  Spores
//
//  Created by Julien Ducret on 27/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)

+ (NSString*)stringFromTimeInteger:(NSInteger)time;

@end
