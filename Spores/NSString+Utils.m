//
//  NSString+Utils.m
//  Spores
//
//  Created by Julien Ducret on 27/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "NSString+Utils.h"

@implementation NSString (Utils)

+ (NSString*)stringFromTimeInteger:(NSInteger)time{
    NSString *minutesString = [NSString stringWithFormat:time/60>9?@"%d":@"0%d",time/60];
    NSString *secondsString = [NSString stringWithFormat:time%60>9?@"%d":@"0%d",time%60];
    return [NSString stringWithFormat:@"%@:%@", minutesString, secondsString];
}

@end
