//
//  SPBackgroundLayer.h
//  Spores
//
//  Created by Julien Ducret on 18/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface SPBackgroundLayer : SKSpriteNode

@property (nonatomic, readwrite, assign) BOOL animated;
@property (nonatomic, readwrite, assign) CGFloat scaleToWorldFactor;

-(void)populate;

@end
