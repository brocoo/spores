//
//  SPBackgroundLayer.m
//  Spores
//
//  Created by Julien Ducret on 18/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPBackgroundLayer.h"

@implementation SPBackgroundLayer

#define NB_FLOATING 15

-(void)populate{
    
    SKTexture *texture = [SKTexture textureWithImageNamed:@"BackgroundFloating"];
    
    for (NSUInteger i=0; i<NB_FLOATING; i++) {
        SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithTexture:texture];
        [sprite setAnchorPoint:CGPointMake(0.5, 0.5)];
        [sprite setScale:(arc4random()%100)/100.0];
        [sprite setAlpha:((arc4random()%50)/50.0)+0.1];
        [sprite setBlendMode:SKBlendModeAlpha];
        [sprite setPosition:CGPointMake((float)(arc4random()%(int)self.size.width)-(int)(self.size.width/2),
                                        (float)(arc4random()%(int)self.size.height)-(int)(self.size.height/2))];
        [self addChild:sprite];
    }
}

@end
