//
//  SPCell.h
//  Spores
//
//  Created by Julien Ducret on 18/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class SPCell;

@protocol SPCellDelegate <NSObject>

- (void)cellSelected:(SPCell*)cell;

- (void)cellDoubleTapped:(SPCell*)cell;

@end

@interface SPCell : SKSpriteNode

@property (nonatomic, readonly, assign) NSInteger sporesCount;
@property (nonatomic, readonly, assign) CGFloat sporesSpawningSpeed;
@property (nonatomic, readonly, assign) CGFloat radius;
@property (nonatomic, readonly, assign) NSUInteger maxSporesCount;
@property (nonatomic, readonly, strong) NSMutableArray *incomingSpores;
@property (nonatomic, readonly, assign) Faction faction;
@property (nonatomic, readwrite, strong) SKSpriteNode *offScreenPointer;
@property (nonatomic, readwrite, weak) id<SPCellDelegate> cellDelegate;
@property (nonatomic, assign) BOOL isSelected;

-(void)becomesTarget;

-(void)update:(double)delta offScreen:(BOOL)isOffScreen;

-(void)setData:(NSDictionary*)data;

-(NSInteger)flockCreated;

@end
