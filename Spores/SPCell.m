//
//  SPCell.m
//  Spores
//
//  Created by Julien Ducret on 18/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPCell.h"
#import "SPSpore.h"
#import "SPSoundManager.h"

@interface SPCell()

@property (nonatomic, strong) SKLabelNode *cellLabelNode;
@property (nonatomic, assign) CGFloat clock;
@property (nonatomic, assign) CGFloat spawnDelay;
@property (nonatomic, strong) NSDictionary *notificationContent;
@property (nonatomic, strong) SKSpriteNode *selectionSprite;
@property (nonatomic, strong) SKSpriteNode *targetSprite;

@end

@implementation SPCell

#pragma mark - Initialization

-(void)setData:(NSDictionary*)data{
    _incomingSpores = [[NSMutableArray alloc] init];
    _faction = [[data objectForKey:@"faction"] intValue];
    _radius = [[data objectForKey:@"radius"] floatValue];
    _sporesCount = [[data objectForKey:@"sporeCount"] intValue];
    _maxSporesCount = [[data objectForKey:@"maxSporeCount"] intValue];
    _spawnDelay = [[data objectForKey:@"spawnDelay"] floatValue];
    [self setPosition:CGPointMake([[data objectForKey:@"x"] floatValue], [[data objectForKey:@"y"] floatValue])];
    [self factionChanged];
    [self setSize:CGSizeMake(_radius*2, _radius*2)];
    [self setUserInteractionEnabled:YES];
    [self setColorBlendFactor:0.6];
    _cellLabelNode = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-UltraLight"];
    [_cellLabelNode setFontSize:_radius/1.5];
    [_cellLabelNode setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [_cellLabelNode setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeCenter];
    [_cellLabelNode setPosition:CGPointMake(0, 0)];
    [_cellLabelNode setText:[NSString stringWithFormat:@"%d",_sporesCount]];
    _isSelected = NO;
    [self addChild:_cellLabelNode];
}

-(void)factionChanged{
    if (_faction == FactionPlayer){
        [self removeAllActions];
        SKAction *colorChange = [SKAction colorizeWithColor:[UIColor sporeCellBlue] colorBlendFactor:1.0 duration:0.3];
        [self runAction: colorChange];
    }else if(_faction == FactionComputer){
        [self removeAllActions];
        SKAction *colorChange = [SKAction colorizeWithColor:[UIColor sporeCellRed] colorBlendFactor:1.0 duration:0.3];
        [self runAction: colorChange];
        if (_isSelected) [self setIsSelected:NO];
    }
}

-(NSInteger)flockCreated{
    if (_sporesCount > 1) {
        // Create a flock targeting the cell
        NSUInteger sporesSent = (_sporesCount/2)>MAXIMUM_FLOCK_SIZE?MAXIMUM_FLOCK_SIZE:(_sporesCount/2);
        _sporesCount -= sporesSent;
        [_cellLabelNode setText:[NSString stringWithFormat:@"%d",_sporesCount]];
        return sporesSent;
    }else{
        return 0;
    }
}

- (void)setIsSelected:(BOOL)isSelected{
    if (isSelected && !_isSelected) {
        
        if (!_selectionSprite) {
            _selectionSprite = [SKSpriteNode spriteNodeWithImageNamed:@"Selection"];
            [_selectionSprite setSize:CGSizeMake(self.size.width+10, self.size.height+10)];
        }else{
            [_selectionSprite removeFromParent];
        }
        
        [self addChild:_selectionSprite];
        [_selectionSprite removeAllActions];
        [_selectionSprite setScale:0.1];
        
        // Start the animations
        SKAction *animation1 = [SKAction scaleTo:1.1 duration:0.15];
        SKAction *animation2 = [SKAction scaleTo:1.0 duration:0.05];
        SKAction *sequence = [SKAction sequence:@[animation1,animation2]];
        [_selectionSprite runAction:sequence];
        
        SKAction *animation3 = [SKAction repeatActionForever:[SKAction rotateByAngle:M_PI*2 duration:4.0]];
        [_selectionSprite runAction:animation3];
        
    }else{
        
        // Start the animation
        SKAction *animation1 = [SKAction scaleTo:0.1 duration:0.15];
        SKAction *animation2 = [SKAction runBlock:^{
            [_selectionSprite removeFromParent];
            [_selectionSprite removeAllActions];
        }];
        SKAction *sequence = [SKAction sequence:@[animation1,animation2]];
        [_selectionSprite runAction:sequence];
        
    }
    _isSelected = isSelected;
}

- (void)becomesTarget{
    
    if (!_targetSprite){
        _targetSprite = [SKSpriteNode spriteNodeWithImageNamed:@"SelectionTarget"];
        [_targetSprite setSize:CGSizeMake(self.size.width+10, self.size.height+10)];
    }
    else{
        [_targetSprite removeAllActions];
        [_targetSprite removeFromParent];
    }
    
    [self addChild:_targetSprite];
    [_targetSprite setScale:0.1];
    
    // Start tehe animations
    SKAction *animation1 = [SKAction scaleTo:1.4 duration:0.25];
    SKAction *animation2 = [SKAction scaleTo:0.1 duration:0.25];
    SKAction *animation3 = [SKAction runBlock:^{
        [_targetSprite removeFromParent];
        [_targetSprite removeAllActions];
    }];
    SKAction *sequence = [SKAction sequence:@[animation1,animation2,animation3]];
    [_targetSprite runAction:sequence];
}

-(void)update:(double)delta offScreen:(BOOL)isOffScreen{
    
    // Build up the spore count
    if (_faction != FactionNeutral) {
        _clock += delta;
        if (_clock > 0.5) {
            if (_sporesCount < _maxSporesCount) {
                _sporesCount += 1;
                //                [self setColorBlendFactor:(float)_sporesCount/((float)_maxSporesCount*2)];
                [_cellLabelNode setText:[NSString stringWithFormat:@"%d",_sporesCount]];
            }else if(_sporesCount > _maxSporesCount){
                _sporesCount -= 1;
                [_cellLabelNode setText:[NSString stringWithFormat:@"%d",_sporesCount]];
            }
            _clock = 0;
        }
    }
    
    // Deal with incoming spores
    CGFloat step = delta*100;
    if (step == 0) return;
    NSUInteger i = 0;
    while (i < _incomingSpores.count) {
        SPSpore *currentSpore = [_incomingSpores objectAtIndex:i];
        SPVector v = SPVectorBetweenPoints(currentSpore.position, self.position);
        double d = SPVectorLength(v);
        if (d < 2) {
            
            if (!isOffScreen) {
                [[SPSoundManager shared] playSporeAttackSoundForCell:self];
            }
            
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(sporeAbsorbed) object:nil];
            [self performSelector:@selector(sporeAbsorbed) withObject:nil afterDelay:0.1];
            
            if (currentSpore.faction != self.faction){
                _sporesCount -= currentSpore.weight;
                if (_sporesCount < 1) {
                    _sporesCount = 0;
                    
                    // Send a notification
                    NSString *message;
                    if (currentSpore.faction == FactionComputer && _faction == FactionPlayer) message = @"One of your cell has been captured";
                    else if(currentSpore.faction == FactionPlayer) message = @"You captured a new cell";
                    else message = @"Computer captured a new cell";
                    
                    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(sendCapturedNotificationWithUserInfo:) object:_notificationContent];
                    _notificationContent = [NSDictionary dictionaryWithObject:message forKey:@"message"];
                    [self performSelector:@selector(sendCapturedNotificationWithUserInfo:) withObject:_notificationContent afterDelay:1.0];
                    
                    _faction = currentSpore.faction;
                    [self factionChanged];
                    
                }
            }else if(_sporesCount < _maxSporesCount*2){
                _sporesCount ++;
            }
            [_cellLabelNode setText:[NSString stringWithFormat:@"%d",_sporesCount]];
            [currentSpore removeFromParent];
            [_incomingSpores removeObject:currentSpore];
        }else{
            CGFloat coeff = d/step;
            [currentSpore setPosition:CGPointMake(currentSpore.position.x + v.x/coeff,
                                                  currentSpore.position.y + v.y/coeff)];
            i++;
        }
    }
}

#pragma mark - Handle user interactions

- (void)singleTouchBegan{
    [_cellDelegate cellSelected:self];
}

- (void)doubleTouchBegan{
    [_cellDelegate cellDoubleTapped:self];
}

- (void)touchesBegan:(NSSet*) touches withEvent:(UIEvent *)event{
    if (event.type == UIEventTypeTouches && touches.count == 1) {
        if (_faction == FactionPlayer) {
            if ([[touches anyObject] tapCount] == 1) {
                [self singleTouchBegan];
            }
            else if ([[touches anyObject] tapCount] == 2 && self.faction == FactionPlayer){
                [self doubleTouchBegan];
            }
        }else{
            [self singleTouchBegan];
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    [(SKSpriteNode*)_cellDelegate touchesMoved:touches withEvent:event];
}

#pragma mark - NSNotification handling

-(void)sendCapturedNotificationWithUserInfo:(NSDictionary*)userInfo{
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCellCaptured object:nil userInfo:userInfo];
}

#pragma mark - SPActions

-(void)sporeAbsorbed{
    SKAction *zoomIn = [SKAction scaleTo: 1.1 duration: 0.05];
    SKAction *zoomOut = [SKAction scaleTo: 1.0 duration: 0.05];
    SKAction *moveSequence = [SKAction sequence:@[zoomIn, zoomOut]];
    [self runAction: moveSequence];
}

-(void)captureAction{
    SKAction *zoomIn = [SKAction scaleTo: 1.2 duration: 0.1];
    SKAction *zoomOut = [SKAction scaleTo: 1.0 duration: 0.1];
    SKAction *moveSequence = [SKAction sequence:@[zoomIn, zoomOut]];
    [self runAction: moveSequence];
}

@end
