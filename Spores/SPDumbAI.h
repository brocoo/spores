//
//  SPDumbAI.h
//  Spores
//
//  Created by Julien Ducret on 09/07/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SPDumbAI;
@class SPWorld;
@class SPCell;

@protocol SPAIDelegate <NSObject>

-(void)AI:(SPDumbAI*)AI performsActionFromCell:(SPCell*)cell toCell:(SPCell*)otherCell;

@end

@interface SPDumbAI : NSObject

@property (nonatomic, weak) id <SPAIDelegate> AIDelegate;

-(id)initWithWorld:(SPWorld*)world andParams:(NSDictionary*)params;

-(BOOL)update:(CGFloat)delta forCells:(NSArray*)cells withOtherCells:(NSArray*)otherCells;

@end
