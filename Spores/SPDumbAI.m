//
//  SPDumbAI.m
//  Spores
//
//  Created by Julien Ducret on 09/07/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPDumbAI.h"
#import "SPCell.h"
#import "SPWorld.h"

@interface SPDumbAI()

@property (nonatomic, assign) CGFloat reactionTime;
@property (nonatomic, assign) CGFloat clock;
@property (nonatomic, weak) SPWorld *world;
@property (nonatomic, assign) BOOL isAgressive;

@end

@implementation SPDumbAI

@synthesize reactionTime = _reactionTime;
@synthesize clock = _clock;
@synthesize AIDelegate = _AIDelegate;
@synthesize world = _world;
@synthesize isAgressive = _isAgressive;

-(id)initWithWorld:(SPWorld*)world andParams:(NSDictionary*)params{
    self = [super init];
    if (self) {
        _reactionTime = [[params objectForKey:@"reactionDelay"] floatValue];
        _isAgressive = [[params objectForKey:@"isAggressive"] boolValue];
        _clock = 0;
        _world = world;
    }
    return self;
}

-(BOOL)update:(CGFloat)delta forCells:(NSArray*)cells withOtherCells:(NSArray*)otherCells{
    
    _clock += delta;
    
    // Time to take a decision
    if (_clock > _reactionTime) {
        
        // Computer should take a random action among the following ones
        NSUInteger decision = arc4random()%2;
        
        SPCell *originCell = nil;
        SPCell *targetedCell = nil;
        
        // Attack a cell
        if (decision == 1 && _isAgressive){
                
                CGFloat distance = -1;
                CGFloat currentDistance = 0;
                // Find the pair of cells with the shortest distance between eachothers
                for (SPCell *cell in cells) {
                    
                    // Security to avoid emptying cells
                    if (cell.sporesCount < cell.maxSporesCount/3) continue;
                    
                    for (SPCell *otherCell in otherCells) {
                        currentDistance = SPVectorLength(SPVectorBetweenPoints(cell.position, otherCell.position));
                        if (currentDistance < distance || distance == -1) {
                            // We got a match !
                            distance = currentDistance;
                            originCell = cell;
                            targetedCell = otherCell;
                        }
                    }
                }
        }
        // Transfer spores from full cells to other ones
        else{
            for (SPCell *cell in cells) {
                if (cell.sporesCount < (cell.maxSporesCount/3)*2) continue;
                
                for (SPCell *othercell in cells) {
                    
                    if(cell == othercell) continue;
                    if(othercell.sporesCount > (othercell.maxSporesCount/3)*2) continue;
                    
                    // We got a match !
                    originCell = cell;
                    targetedCell = othercell;
                }
            }
        }
        
        
        if (originCell && targetedCell) {
            [_AIDelegate AI:self performsActionFromCell:originCell toCell:targetedCell];
        }
        
        _clock = 0;
    }
    
    return NO;
}

@end
