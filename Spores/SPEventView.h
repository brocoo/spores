//
//  SPEventView.h
//  Spores
//
//  Created by Julien Ducret on 20/07/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPGamePopView.h"
@class SPEventView;

@protocol SPEventViewDelegate <NSObject>

-(void)eventViewNextButtonTapped:(SPEventView*)eventView;

-(void)eventViewSkipButtonTapped:(SPEventView*)eventView;

@end

@interface SPEventView : SPGamePopView

// Outlets
@property (nonatomic, weak) IBOutlet UIImageView * imageView;
@property (nonatomic, weak) IBOutlet UILabel * subTitleLabel;
@property (nonatomic, weak) IBOutlet UIButton * nextButton;
@property (nonatomic, weak) IBOutlet UIButton * skipButton;
@property (nonatomic, weak) id<SPEventViewDelegate> eventViewDelegate;

@end
