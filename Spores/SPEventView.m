//
//  SPEventView.m
//  Spores
//
//  Created by Julien Ducret on 20/07/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPEventView.h"

@implementation SPEventView

@synthesize imageView = _imageView;
@synthesize subTitleLabel = _subTitleLabel;
@synthesize nextButton = _nextButton;
@synthesize skipButton = _skipButton;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma mark - UIButton action methods

- (IBAction)nextButtonTapped:(UIButton*)sender{
    [_eventViewDelegate eventViewNextButtonTapped:self];
}

- (IBAction)skipButtonTapped:(UIButton*)sender{
    [_eventViewDelegate eventViewSkipButtonTapped:self];
}

-(void)showAnimated:(BOOL)animated withCompletion:(CompletionBlock)completion{
    
    if (animated) {
        // Position the view in its initial state
        [self setCenter:CGPointMake(S_WIDTH+self.frame.size.width/2, S_HEIGHT/2)];
        [self setAlpha:0.0];
        
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [self setCenter:CGPointMake(S_WIDTH/2, S_HEIGHT/2)];
                             [self setAlpha:1.0];
                         }
                         completion:^(BOOL finished) {
                             if (completion)completion();
                         }
         ];
    }else{
        [self setCenter:CGPointMake(S_WIDTH/2, S_HEIGHT/2)];
        [self setAlpha:1.0];
        if (completion)completion();
    }
}

-(void)hideAnimated:(BOOL)animated withCompletion:(CompletionBlock)completion{
    
    if (animated) {
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [self setCenter:CGPointMake(-self.frame.size.width/2, S_HEIGHT/2)];
                             [self setAlpha:0.0];
                         }
                         completion:^(BOOL finished) {
                             if (completion) completion();
                         }
         ];
    }else{
        [self setCenter:CGPointMake(S_WIDTH+self.frame.size.width/2, S_HEIGHT/2)];
        [self setAlpha:0.0];
        if (completion) completion();
    }
}

@end
