//
//  SPFacebookManager.h
//  Spores
//
//  Created by Julien Ducret on 24/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>
@class SPFacebookManager;

@protocol SPFacebookManagerDelegate <NSObject>

@end

@interface SPFacebookManager : NSObject

@property (strong, nonatomic, readonly) FBSession *session;
@property (weak, nonatomic) id<SPFacebookManagerDelegate> delegate;

+ (id)shared;

- (void)applicationDidBecomeActive;

- (void)applicationWillTerminate;

- (BOOL)applicationOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication;

- (void)shareTime:(NSInteger)time forLevel:(NSInteger)level fromViewController:(UIViewController*)viewController;

@end
