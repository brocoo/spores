//
//  SPFacebookManager.m
//  Spores
//
//  Created by Julien Ducret on 24/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPFacebookManager.h"
#import "SPLoadingView.h"

@implementation SPFacebookManager

#pragma mark - Singleton

+ (id)shared
{
    static id shared;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

#pragma mark - Application lifecycle handlers

- (void)applicationDidBecomeActive{
    [FBAppEvents activateApp];
    [FBAppCall handleDidBecomeActiveWithSession:_session];
}

- (void)applicationWillTerminate{
    [_session close];
}

- (BOOL)applicationOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication{
    // attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                        withSession:_session];
}

#pragma mark - Facebook handlers

- (void)publishOnFacebookPostWithBlock:(void(^)(void))facebookBlock andFailureBlock:(void(^)(void))failure{
    if([[FBSession activeSession] isOpen]){
        facebookBlock();
    }else{
        // Ask for publish_actions permissions in context
        if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
            // Permission hasn't been granted, so ask for publish_actions
            [FBSession openActiveSessionWithPublishPermissions:@[@"publish_actions"] defaultAudience:FBSessionDefaultAudienceFriends allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                if (FBSession.activeSession.isOpen && !error) {
                    // Publish the story if permission was granted
                    [FBSession setActiveSession:session];
                    facebookBlock();
                }else{
                    failure();
                }
            }];
        } else {
            // If permissions present, publish the story
            facebookBlock();
        }
    }
}


- (void)shareTime:(NSInteger)time forLevel:(NSInteger)level fromViewController:(UIViewController*)viewController{
    
    // Display the loading view
    SPLoadingView *loadingView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([SPLoadingView class]) owner:nil options:nil] firstObject];
    [viewController.view addSubview:loadingView];
    [[loadingView loadingLabel] setText:@"Loading..."];
    [loadingView showAnimated:YES withCompletion:nil];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    [self publishOnFacebookPostWithBlock:^{
        
        NSString *timeString;
        if (time<60) timeString = [NSString stringWithFormat:@"%d seconds",time];
        else timeString = [NSString stringWithFormat:@"%d minutes and %d",time/60, time%60];
        
        NSDictionary *postParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"http://broco.fr/spores/thumbnail.png",@"picture",
                                    @"http://broco.fr",@"link",
                                    @"Get Spores on your iPhone",@"caption",
                                    [NSString stringWithFormat:@"I just finished the level %d in %@. Try to beat me!",level,timeString],@"description", nil];
        
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:postParams
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      
                                                      [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                      [loadingView hideAnimated:YES withCompletion:^{
                                                          [loadingView removeFromSuperview];
                                                      }];
                                                      
                                                      if (error) {
                                                          NSString *alertText;
                                                          alertText = @"Something went wrong. Try again another time...";
                                                          [[[UIAlertView alloc] initWithTitle:@"Oups" message:alertText delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
                                                      }
                                                  }
         ];
    } andFailureBlock:^{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [loadingView hideAnimated:YES withCompletion:^{
            [loadingView removeFromSuperview];
        }];
        NSString *alertText;
        alertText = @"Something went wrong. Try again another time...";
        [[[UIAlertView alloc] initWithTitle:@"Oups" message:alertText delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    }];
}

@end
