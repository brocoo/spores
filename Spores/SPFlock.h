//
//  SPFlock.h
//  Spores
//
//  Created by Julien Ducret on 20/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class SPCell;
@class SPWorld;

typedef enum FlockStatus:NSUInteger {
    FlockSpawning = 0,
    FlockMoving = 1,
    FlockAttacking = 2
} FlockStatus;

@interface SPFlock : NSObject

@property (nonatomic, readonly, weak) SPWorld *world;
@property (nonatomic, readwrite, weak) SPCell *destination;
@property (nonatomic, readwrite, weak) SPCell *origin;
@property (nonatomic, readwrite, assign) CGFloat speed; // Points per seconds
@property (nonatomic, readonly, assign) NSUInteger sporesCount;
@property (nonatomic, readwrite, assign) FlockStatus status;
@property (nonatomic, readwrite, assign) Faction faction;

-(id)initFromWorld:(SPWorld*)world;

-(void)spawnSpores:(NSUInteger)sporesCount;

-(BOOL)update:(double)delta withDecay:(BOOL)decay andFlocking:(BOOL)flocking;

@end
