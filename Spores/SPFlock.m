//
//  SPFlock.m
//  Spores
//
//  Created by Julien Ducret on 20/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPFlock.h"
#import "SPSpore.h"
#import "SPCell.h"
#import "SPWorld.h"
#import "SPTextureManager.h"

@interface SPFlock()

@property (nonatomic, strong) NSMutableArray *spores;

@end

@implementation SPFlock

@synthesize destination = _destination;
@synthesize speed = _speed;
@synthesize sporesCount = _sporesCount;
@synthesize status = _status;
@synthesize spores = _spores;
@synthesize faction = _faction;
@synthesize world = _world;
@synthesize origin = _origin;

#pragma mark - SPFlock methods


-(id)initFromWorld:(SPWorld*)world{
    self= [super init];
    if (self) {
        _world = world;
    }
    return self;
}

#define MAX_SPORES_PER_FLOCK    7

-(void)spawnSpores:(NSUInteger)sporesCount{
    _sporesCount = sporesCount;
    _status = FlockSpawning;
    
    NSInteger bigSporesNb;// = sporesCount>MAX_SPORES_PER_FLOCK?sporesCount - MAX_SPORES_PER_FLOCK:0;
    NSInteger totalSporesNb;
    
    if (sporesCount <= MAX_SPORES_PER_FLOCK) {
        bigSporesNb = 0;
        totalSporesNb = sporesCount;
    }else if(sporesCount <= MAX_SPORES_PER_FLOCK*2){
        bigSporesNb = sporesCount - MAX_SPORES_PER_FLOCK;
        totalSporesNb = MAX_SPORES_PER_FLOCK;
    }else{
        bigSporesNb = sporesCount / 2;
        totalSporesNb = sporesCount;
    }
    
    SKTexture *texture = [[SPTextureManager sharedManager] sporeTexture];
    
    // Spawn spores
    _spores = [NSMutableArray arrayWithCapacity:sporesCount];
    for (NSUInteger i=0; i<totalSporesNb; i++) {
        SPSpore *spore = [[SPSpore alloc] initWithTexture:texture];
        
        NSInteger diameter = _origin.radius*2;
        NSInteger x = arc4random()%diameter - diameter/2;
        NSInteger y = arc4random()%diameter - diameter/2;
        [spore setPosition:CGPointMake(x + _origin.position.x, y + _origin.position.y)];
        [_spores addObject:spore];
        [spore setVelocity:SPVectorMake(0, 0)];
        [spore setFaction:self.faction];
        [spore setLifespan:3.0f+((arc4random()%50)/10.0f)];
        [spore setAge:0.0f];
        [spore setUserInteractionEnabled:YES];
        if (spore.faction == FactionPlayer) [spore setColor:[UIColor blueColor]];
        else [spore setColor:[UIColor redColor]];
        [spore setColorBlendFactor:0.6];
        
        if (bigSporesNb > 0) {
            [spore setWeight:2];
            [spore setScale:1.5];
            bigSporesNb--;
        }else{
            [spore setWeight:1];
            [spore setScale:1];
        }
        
        [_world addChild:spore];
    }
}

-(BOOL)update:(double)delta withDecay:(BOOL)decay andFlocking:(BOOL)flocking{
    
    NSInteger j = 0;
    // Compute the new position of the spores inside the flock
    for (SPSpore *currentSpore in _spores) {
        
        // Check if we skip computing flocking vectors (to save CPU)
        if (flocking || (currentSpore.velocity.x==0 && currentSpore.velocity.y==0)) {
        
            // Apply decay
            if(decay){
                CGFloat lifespan = currentSpore.lifespan;
                CGFloat age = currentSpore.age + delta;
                
                CGFloat alpha = age * (-1.0f/((1.0f-0.95f)*lifespan)) + (lifespan/((1.0f-0.95f)*lifespan));
                alpha = alpha>1?1:(alpha>0)?alpha:0;
                
                [currentSpore setAlpha:alpha];
                currentSpore.age = age;
            }
            
            // Flocking algorithm
            SPVector r1 = [self cohesionForSpore:currentSpore andFactor:10000];
            SPVector r2 = [self separationForSpore:currentSpore andDistance:12 andFactor:60];
            SPVector r3 = [self velocityForSpore:currentSpore andFactor:1000];
            SPVector r4 = [self tendencyForSpore:currentSpore andFactor:500];
            SPVector r5 = [self awayFromOtherCellsForCurrentSpore:currentSpore andFactor:10];
            [currentSpore setVelocity:SPVectorMake(currentSpore.velocity.x+r1.x+r2.x+r3.x+r4.x+r5.x,
                                                   currentSpore.velocity.y+r1.y+r2.y+r3.y+r4.y+r5.y)];
        }
        
        // Limit the spore speed
        [self limitVelocityForSpore:currentSpore andLimit:1.25];
        [currentSpore setFuturePosition:CGPointMake((currentSpore.position.x + currentSpore.velocity.x),
                                                    (currentSpore.position.y + currentSpore.velocity.y))];
        j++;
    }
    
    // Apply new positions and check decay
    NSUInteger i = 0;
    while (i < _spores.count) {
        SPSpore * currentSpore = [_spores objectAtIndex:i];
        [currentSpore setPosition:CGPointMake(currentSpore.futurePosition.x,
                                              currentSpore.futurePosition.y)];
        if (SPVectorLength(SPVectorBetweenPoints(currentSpore.position, _destination.position))<_destination.radius+10) {
            // Spore is close enough, remove it from the flock and transfer it to the targeted cell
            [_spores removeObject:currentSpore];
            [currentSpore setTargetedCell:_destination];
            [_destination.incomingSpores addObject:currentSpore];
        }else if(isnan(currentSpore.position.x) || isnan(currentSpore.position.y)){
            [currentSpore removeFromParent];
            [_spores removeObject:currentSpore];
        }else if ([currentSpore age] > [currentSpore lifespan]){
            [currentSpore removeFromParent];
            [_spores removeObject:currentSpore];
        }else{
            // Carry on
            i++;
        }
    }
    
    if (_spores.count == 0) {
        return NO;
    }else{
        return YES;
    }
}

#pragma mark - Flocking algorithm methods

#define MIN_VALUE   0.0001

- (SPVector)cohesionForSpore:(SPSpore*)currentSpore andFactor:(CGFloat)factor{
    // Rule 1
    SPVector v = SPVectorMakeZero(); // Center of mass (here actual center of the flock)
    if (_sporesCount) return SPVectorMakeZero();
    for (SPSpore *spore in _spores) {
        if (spore != currentSpore) {
            v.x = v.x + spore.position.x;
            v.y = v.y + spore.position.y;            
        }
    }
    v.x = v.x / (_sporesCount-1);
    v.y = v.y / (_sporesCount-1);
    return SPVectorMake((v.x - currentSpore.position.x)/factor,
                        (v.y - currentSpore.position.y)/factor);
}

- (SPVector)separationForSpore:(SPSpore*)currentSpore andDistance:(CGFloat)distance andFactor:(CGFloat)factor{
    // Rule 2
    SPVector v = SPVectorMakeZero();
    for (SPSpore *spore in _spores) {
        if (spore != currentSpore) {
            if (distanceBetweenPoints(currentSpore.position, spore.position) < distance) {
                v.x = v.x - (spore.position.x - currentSpore.position.x)/factor;
                v.y = v.y - (spore.position.y - currentSpore.position.y)/factor;
            }
        }
    }
    return v;
}

- (SPVector)velocityForSpore:(SPSpore*)currentSpore andFactor:(CGFloat)factor{
    // Rule 3
    SPVector v = SPVectorMakeZero();
    for (SPSpore *spore in _spores) {
        if (spore != currentSpore) {
            v.x = v.x + spore.velocity.x;
            v.y = v.y + spore.velocity.y;
        }
    }
    v.x = v.x / (_sporesCount-1);
    v.y = v.y / (_sporesCount-1);
    return SPVectorMake((v.x - currentSpore.velocity.x)/factor,
                        (v.y - currentSpore.velocity.y)/factor);
}

- (void)limitVelocityForSpore:(SPSpore*)currentSpore andLimit:(CGFloat)limit{
    CGFloat v = SPVectorLength(currentSpore.velocity);
    if (v > limit) {
        [currentSpore setVelocity:SPVectorMake((currentSpore.velocity.x / v)*limit,
                                               (currentSpore.velocity.y / v)*limit)];
    }
}

- (SPVector)awayFromOtherCellsForCurrentSpore:(SPSpore*)currentSpore andFactor:(CGFloat)factor{
    // Rule 2
    SPVector __block v = SPVectorMakeZero();
    [_world enumerateChildNodesWithName:@"Cell" usingBlock:^(SKNode *node, BOOL *stop) {
        SPCell *cell = (SPCell*)node;
        CGFloat d = distanceBetweenPoints(currentSpore.position, cell.position);
        if (cell != _origin && cell != _destination && d<(cell.radius*2)) {
            v.x = v.x - (cell.position.x - currentSpore.position.x)/(factor*(d-cell.radius));
            v.y = v.y - (cell.position.y - currentSpore.position.y)/(factor*(d-cell.radius));
        }
    }];
    return v;
}

- (SPVector)tendencyForSpore:(SPSpore*)currentSpore andFactor:(CGFloat)factor{
    return SPVectorMake((_destination.position.x-currentSpore.position.x)/factor,
                        (_destination.position.y-currentSpore.position.y)/factor);
}

@end
