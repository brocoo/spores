//
//  SPGameKitManager.h
//  Spores
//
//  Created by Julien Ducret on 30/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPGameKitManager : NSObject

+ (id)shared;

- (void)authenticateLocalPlayerFromViewController:(UIViewController*)currentViewController
                                      showLoginUI:(BOOL)showLoginUI
                                       withSucces:(CompletionSuccessBlock)success
                                       andFailure:(CompletionFailureBlock)failure;

- (void)reportTime:(NSInteger)time forLevel:(NSInteger)level;

@end
