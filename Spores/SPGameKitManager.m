//
//  SPGameKitManager.m
//  Spores
//
//  Created by Julien Ducret on 30/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPGameKitManager.h"
#import <GameKit/GameKit.h>

@implementation SPGameKitManager

+ (id)shared{
    static SPGameKitManager *gameKitManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        gameKitManager = [[SPGameKitManager alloc] init];
    });
    return gameKitManager;
}

- (void)authenticateLocalPlayerFromViewController:(UIViewController*)currentViewController
                                      showLoginUI:(BOOL)showLoginUI
                                       withSucces:(CompletionSuccessBlock)success
                                       andFailure:(CompletionFailureBlock)failure{
    
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    if ([localPlayer isAuthenticated]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kGameCenterAuthenticated];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if (success) success(nil,YES);
    }else{
        localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
            
            if (viewController != nil && showLoginUI) {
                
                [currentViewController presentViewController:viewController animated:YES completion:nil];
                
            } else if ([GKLocalPlayer localPlayer].isAuthenticated) {
                
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kGameCenterAuthenticated];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                if (success) success(nil, YES);
                
            } else if (error != nil) {
                
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kGameCenterAuthenticated];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                if (failure) failure(error);
                
            }
        };
    }
}

- (void)reportTime:(NSInteger)time forLevel:(NSInteger)level{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kGameCenterShouldShare] && [[NSUserDefaults standardUserDefaults] boolForKey:kGameCenterShouldShare]) {
        GKScore *scoreReporter = [[GKScore alloc] initWithLeaderboardIdentifier:[NSString stringWithFormat:level<10?@"level0%d":@"level%d",level]];
        [scoreReporter setValue:(int64_t)time];
        [scoreReporter setContext:0];
        [scoreReporter reportScoreWithCompletionHandler:^(NSError *error) {
            if (error) {
                NSLog(@"Error while reporting score on GameCenter");
            }else{
                NSLog(@"Score reported succesfully");
            }
        }];
    }
}

@end
