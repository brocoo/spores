//
//  SPGameNotificationManager.h
//  Spores
//
//  Created by Julien Ducret on 04/07/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SPGameNotificationManager;

@protocol SPGameNotificationManagerDelegate <NSObject>

-(BOOL)notificationManager:(SPGameNotificationManager*)notificationManager displayTopNotificationMessage:(NSString*)message;

@end

@interface SPGameNotificationManager : NSObject

@property (nonatomic, weak) id <SPGameNotificationManagerDelegate> notificationDelegate;

-(void)dequeueNotifications;

@end
