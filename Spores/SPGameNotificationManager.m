//
//  SPGameNotificationManager.m
//  Spores
//
//  Created by Julien Ducret on 04/07/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPGameNotificationManager.h"

@interface SPGameNotificationManager()

@property (nonatomic, assign) CGFloat notificationTimeAlive;
@property (nonatomic, strong) NSMutableArray *notificationsArray;

@end

@implementation SPGameNotificationManager

@synthesize notificationDelegate = _notificationDelegate;
@synthesize notificationTimeAlive = _notificationTimeAlive;
@synthesize notificationsArray = _notificationsArray;

-(id)init{
    self = [super init];
    if (self) {
        _notificationsArray = [[NSMutableArray alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationReceived:) name:kNotificationCellCaptured object:nil];
    }
    return self;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)notificationReceived:(NSNotification*)notification{
    
    // If notifications are enabled
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kTopNotificationsEnabled]) {
    
        // If the queue is empty,
        if ([_notificationsArray count] == 0) {
            NSString *message = [notification.userInfo objectForKey:@"message"];
            [_notificationDelegate notificationManager:self displayTopNotificationMessage:message];
        }
        
        // Add the notification in the queue
        [_notificationsArray insertObject:notification.userInfo atIndex:0];
    }
}

-(void)dequeueNotifications{
    [_notificationsArray removeLastObject];
    if (_notificationsArray.count > 0) {
        NSString *message = [[_notificationsArray lastObject] objectForKey:@"message"];
        [_notificationDelegate notificationManager:self displayTopNotificationMessage:message];
    }
}

@end
