//
//  SPGameOverMenu.h
//  Spores
//
//  Created by Julien Ducret on 18/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPGamePopView.h"
@class SPGameOverMenu;

@protocol SPGameOverMenuDelegate <NSObject>

-(void)gameOverMenuNextLevelTapped:(SPGameOverMenu*)menu;
-(void)gameOverMenuRestartLevelTapped:(SPGameOverMenu*)menu;
-(void)gameOverMenuBackTapped:(SPGameOverMenu*)menu;
-(void)gameOverMenuFacebookTapped:(SPGameOverMenu *)menu isBestScore:(BOOL)isBestScore;
-(void)gameOverMenuTwitterTapped:(SPGameOverMenu*)menu isBestScore:(BOOL)isBestScore;

@end

@interface SPGameOverMenu : SPGamePopView

@property (nonatomic, assign) id<SPGameOverMenuDelegate> gameOverMenuDelegate;

- (IBAction)nextLevelTapped:(UIButton*)sender;
- (IBAction)restartLevelTapped:(UIButton*)sender;
- (IBAction)backToMenuTapped:(UIButton*)sender;

@end
