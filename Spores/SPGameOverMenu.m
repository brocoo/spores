//
//  SPGameOverMenu.m
//  Spores
//
//  Created by Julien Ducret on 18/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPGameOverMenu.h"

@implementation SPGameOverMenu

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)showAnimated:(BOOL)animated withCompletion:(CompletionBlock)completion{
    [self setCenter:CGPointMake(S_WIDTH/2 + S_WIDTH, S_HEIGHT/2)];
    [UIView animateWithDuration:0.15 animations:^{
        [self setCenter:CGPointMake(S_WIDTH/2, S_HEIGHT/2)];
    } completion:^(BOOL finished) {
        if (completion) completion();
    }];
}

-(void)hideAnimated:(BOOL)animated withCompletion:(CompletionBlock)completion{
    [UIView animateWithDuration:0.15 animations:^{
        [self setCenter:CGPointMake(-S_WIDTH/2, S_HEIGHT/2)];
    } completion:^(BOOL finished) {
        if (completion) completion();
    }];
}

#pragma mark - UIButton action methods

- (void)nextLevelTapped:(UIButton*)sender{
    [_gameOverMenuDelegate gameOverMenuNextLevelTapped:self];
}

- (void)restartLevelTapped:(UIButton*)sender{
    [_gameOverMenuDelegate gameOverMenuRestartLevelTapped:self];
    
}

- (void)backToMenuTapped:(UIButton*)sender{
    [_gameOverMenuDelegate gameOverMenuBackTapped:self];
}

@end
