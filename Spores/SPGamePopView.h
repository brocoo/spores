//
//  SPGamePopView.h
//  Spores
//
//  Created by Julien Ducret on 28/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPGamePopView : UIView

-(void)showAnimated:(BOOL)animated withCompletion:(CompletionBlock)completion;

-(void)hideAnimated:(BOOL)animated withCompletion:(CompletionBlock)completion;

@end
