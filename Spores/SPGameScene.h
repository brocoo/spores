//
//  SPGameScene.h
//  Spores
//
//  Created by Julien Ducret on 15/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
@class SPGameScene;

enum GameStatus {
    GamePaused = 0,
    GameRunning = 1,
    GameInitialized = 2
};
typedef enum GameStatus GameStatus;

@protocol SPGameSceneDelegate <NSObject>

- (void)gameScene:(SPGameScene*)gameScene gameClockUpdated:(NSUInteger)time;

@end

@interface SPGameScene : SKScene

@property (nonatomic, assign) GameStatus gameStatus;
@property (nonatomic, readwrite, strong) NSDictionary *levelContent;
@property (nonatomic, weak) id<SPGameSceneDelegate> gameSceneDelegate;

-(void)skipNextGameEvents;

-(void)gameEventDismissed;

@end
