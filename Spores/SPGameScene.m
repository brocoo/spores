//
//  SPGameScene.m
//  Spores
//
//  Created by Julien Ducret on 15/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPGameScene.h"
#import "SPSpore.h"
#import "SPCell.h"
#import "SPBackgroundLayer.h"
#import "SPWorld.h"
#import "SPFlock.h"
#import "SPDumbAI.h"
#import "SPRegisteredGameEvent.h"
#import <math.h>

@interface SPGameScene () <SPCellDelegate, SPAIDelegate>

@property BOOL contentCreated;
@property (nonatomic, strong) SPBackgroundLayer *background;
@property (nonatomic, strong) SPBackgroundLayer *backgroundOverlay;
@property (nonatomic, strong) SPWorld *world;
@property (nonatomic, assign) double previousTime;
@property (nonatomic, strong) NSMutableArray *flocks;
@property (nonatomic, assign) SPCell *currentSelectedCell;
@property (nonatomic, strong) SPDumbAI *dumbAI;
@property (nonatomic, strong) NSMutableArray *events;
@property (nonatomic, assign) BOOL isDecayAllowed;

// Timer properties
@property (nonatomic, assign) CGFloat gameTimer;

// User touch related properties
@property (nonatomic, assign) BOOL isMoving;
@property (nonatomic, assign) CGPoint previousDragDelta;
@property (nonatomic, assign) CGFloat initialDistanceBetweenTouches;
@property (nonatomic, assign) CGPoint centerOfZoomingInWorld;
@property (nonatomic, assign) CGPoint worldLocationBeforeZoom;
@property (nonatomic, assign) CGFloat minZoomFactor;
@property (nonatomic, assign) CGFloat previousWorldScale;
@property (nonatomic, assign) BOOL twoFingersDetected;

@end

@implementation SPGameScene

@synthesize contentCreated = _contentCreated;
@synthesize background= _background;
@synthesize backgroundOverlay = _backgroundOverlay;
@synthesize world = _world;
@synthesize previousTime = _previousTime;
@synthesize gameStatus = _gameStatus;
@synthesize flocks = _flocks;
@synthesize levelContent = _levelContent;
@synthesize dumbAI = _dumbAI;
@synthesize events = _events;
@synthesize isDecayAllowed = _isDecayAllowed;
@synthesize isMoving = _isMoving;
@synthesize initialDistanceBetweenTouches = _initialDistanceBetweenTouches;
@synthesize centerOfZoomingInWorld = _centerOfZoomingInWorld;
@synthesize worldLocationBeforeZoom = _worldLocationBeforeZoom;
@synthesize minZoomFactor = _minZoomFactor;
@synthesize previousWorldScale = _previousWorldScale;
@synthesize previousDragDelta = _previousDragDelta;
@synthesize twoFingersDetected = _twoFingersDetected;
@synthesize gameSceneDelegate = _gameSceneDelegate;
@synthesize gameTimer = _gameTimer;

#pragma mark - Initialize content

- (void)didMoveToView:(SKView *)view{
    if (!_contentCreated) {
        [self createSceneContents];
        _contentCreated = YES;
        _previousTime = 0;
        _gameStatus = GameInitialized;
        _flocks = [[NSMutableArray alloc] init];
    }
}

- (void)createSceneContents
{
    self.scaleMode = SKSceneScaleModeAspectFit;
    
    if (!_levelContent) return;
    
    // Create world
    NSDictionary *worldData = [_levelContent objectForKey:kWorld];
    _world = [SPWorld node];
    [_world setName:kWorld];
    [_world setSize:CGSizeMake([[[worldData objectForKey:kSize] objectForKey:kWidth] floatValue],
                               [[[worldData objectForKey:kSize] objectForKey:kHeight] floatValue])];
    [_world setPosition:CGPointMake([[[worldData objectForKey:kCamera] objectForKey:kX] floatValue],
                                    [[[worldData objectForKey:kCamera] objectForKey:kY] floatValue])];
    [_world createWorld];
    _previousWorldScale = 1;
    _twoFingersDetected = NO;
    
    // Define the minimum zoom factor in a way that
    // world's boundaries can't be smaller than the view frame
    _minZoomFactor = MIN(self.view.frame.size.width / _world.size.width,
                                self.view.frame.size.height / _world.size.height);
    _minZoomFactor = _minZoomFactor>1?1:_minZoomFactor;
    
    // Create background
    _background = [[SPBackgroundLayer alloc] init];
    [_background setName:@"Background"];
    [_background setSize:CGSizeMake(_world.size.width,
                                    _world.size.height)];
    [_background setAnchorPoint:CGPointMake(0.5, 0.5)];
    [_background setScaleToWorldFactor:0.6];
    [_background setPosition:CGPointMake(CGRectGetMidX(self.frame),
                                         CGRectGetMidY(self.frame))];
    [_background populate];
    [_background setPaused:YES];
    
    // Add the childen in the right order
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"Background01"];
    [background setSize:self.size];
    [background setAnchorPoint:CGPointMake(0.5, 0.5)];
    [background setPosition:CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))];
    [background setBlendMode:SKBlendModeReplace];
    
    [self addChild:background];
    [self addChild:_background];
    [self addChild:_world];
    
    // Add cells
    NSArray *cells = [[_levelContent objectForKey:kContent] objectForKey:kCells];
    for (NSDictionary *cellDictionary in cells) {
        
        SPCell *cell = [SPCell spriteNodeWithImageNamed:@"Cell"];
        [cell setName:@"Cell"];
        [_world addChild:cell];
        [cell setData:cellDictionary];
        [cell setCellDelegate:self];
    }
    
    // Check is spore decay is enabled
    _isDecayAllowed = [[_levelContent objectForKey:kAllowDecay] boolValue];
    
    // Init the AI
    _dumbAI = [[SPDumbAI alloc] initWithWorld:_world andParams:[_levelContent objectForKey:kAI]];
    [_dumbAI setAIDelegate:self];
    
    // Populate the events array
    _events = [NSMutableArray array];
    for (NSDictionary *event in [_levelContent objectForKey:kEvents]) {
        SPRegisteredGameEvent *gameEvent = [[SPRegisteredGameEvent alloc] initWithContent:event];
        [_events addObject:gameEvent];
    }
}

#pragma mark - SPCellDelegate methods

- (void)cellSelected:(SPCell *)cell{
    // A cell has already been selected
    // This will launch a flock on the target
    if (_currentSelectedCell && _currentSelectedCell!=cell) {
        
        SPFlock *flock = [[SPFlock alloc] initFromWorld:_world];
        [flock setOrigin:_currentSelectedCell];
        [flock setDestination:cell];
        [flock setFaction:_currentSelectedCell.faction];
        [flock setSpeed:4];
        [flock spawnSpores:[_currentSelectedCell flockCreated]];
        [_flocks addObject:flock];

        [cell becomesTarget];
        
        // Keep the current cell selected ?
//        [_currentSelectedCell setIsSelected:NO];
//        _currentSelectedCell = nil;
        
    }
    // We deselect the current cell
    else if (_currentSelectedCell==cell){
        [_currentSelectedCell setIsSelected:NO];
        _currentSelectedCell = nil;
    }
    // First selection
    else if(cell.faction == FactionPlayer){
        _currentSelectedCell = cell;
        [cell setIsSelected:YES];
    }
}

- (void)cellDoubleTapped:(SPCell *)cell{
    // If the cell is owned by the player,
    if(cell != _currentSelectedCell && cell.faction == FactionPlayer) {
        [_currentSelectedCell setIsSelected:NO];
        _currentSelectedCell = nil;
        _currentSelectedCell = cell;
        [cell setIsSelected:YES];
    }
}

#pragma mark - Handle user interactions

- (void)zoomBeganWithTouches:(NSSet*)touches{
    _isMoving = NO;
    CGPoint touch1Location = [[[touches allObjects] objectAtIndex:0] locationInView:self.view];
    CGPoint touch2Location = [[[touches allObjects] objectAtIndex:1] locationInView:self.view];
    _initialDistanceBetweenTouches = distanceBetweenPoints(touch1Location, touch2Location);
    
    CGPoint centerOfZoomingInView = SPVectorPointBetweenPoints(touch1Location, touch2Location);
    
    CGFloat worldY = self.frame.size.height - centerOfZoomingInView.y; // Y axis is inverted in the world
    _centerOfZoomingInWorld = CGPointMake((centerOfZoomingInView.x) - (_world.position.x/_world.scale),
                                          worldY - (_world.position.y/_world.scale));
    
    _worldLocationBeforeZoom = _world.position;
    _previousWorldScale = _world.scale;
}

- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent *)event
{
    if (event.type == UIEventTypeTouches) {
        
        if (event.type == UIEventTypeTouches && touches.count == 1) {
            _isMoving = NO;
            _previousDragDelta = CGPointZero;
            _twoFingersDetected = NO;
        }
        
        else if (event.type == UIEventTypeTouches && touches.count == 2){
            
            [self zoomBeganWithTouches:touches];
            _twoFingersDetected = YES;
        }
    }
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    
    // Dragging
    if (event.type == UIEventTypeTouches && touches.count == 1) {
    
        CGPoint oldTouchPosition = [[[touches allObjects] firstObject] previousLocationInView:self.view];
        CGPoint newTouchPosition = [[[touches allObjects] firstObject] locationInView:self.view];
        CGFloat deltaX = newTouchPosition.x - oldTouchPosition.x;
        CGFloat deltaY = newTouchPosition.y - oldTouchPosition.y;
        
        // Compute new world coordinates
        CGFloat newWorldX = _world.position.x + deltaX;
        CGFloat newWorldY = _world.position.y - deltaY; // Y axis is inverted
        
        // Make sure we're still inside the world's boundaries
        if ( (newWorldX >= 0 && _previousDragDelta.x < deltaX ) || ((S_WIDTH - _world.size.width*_world.scale) > newWorldX  && _previousDragDelta.x > deltaX) ){
            deltaX = 0;
            newWorldX = _world.position.x;
        }
        if ( (newWorldY >= 0 && _previousDragDelta.y > deltaY ) || ((S_HEIGHT - _world.size.height*_world.scale) > newWorldY && _previousDragDelta.y < deltaY) ){
            deltaY = 0;
            newWorldY = _world.position.y;
        }
        // Move the world
        [_world setPosition:CGPointMake(newWorldX, newWorldY)];
        
        // Move background layer
        CGFloat newBackgroundX = _background.position.x + (deltaX * _background.scaleToWorldFactor);
        CGFloat newBackgroundY = _background.position.y - (deltaY * _background.scaleToWorldFactor); // Y axis is inverted
        [_background setPosition:CGPointMake(newBackgroundX, newBackgroundY)];
        
        _previousDragDelta = CGPointMake(deltaX, deltaY);
        _isMoving = YES;
    }
    // Zooming
    else if (event.type == UIEventTypeTouches && touches.count == 2){
        
        // If the user put one finger after another on the screen, the zoom initialization hasn't been detected yet
        // Let's do that first then.
        if (!_twoFingersDetected) {
            
            [self zoomBeganWithTouches:touches];
            _twoFingersDetected = YES;
            
        }
        // Two fingers have been previously detected, carry on with the zooming
        else{
            
            CGPoint touch1NewLocation = [[[touches allObjects] objectAtIndex:0] locationInView:self.view];
            CGPoint touch2NewLocation = [[[touches allObjects] objectAtIndex:1] locationInView:self.view];
            
            CGFloat zoomFactor = distanceBetweenPoints(touch1NewLocation, touch2NewLocation) / _initialDistanceBetweenTouches * _previousWorldScale;
            
            // Define the zoom value between boundaries
            zoomFactor = zoomFactor>2.0?2.0:zoomFactor<_minZoomFactor?_minZoomFactor:zoomFactor;
            
            // Scale the world
            [_world setScale:zoomFactor];
            
            // Re-capture the world coordinates of the center of zooming after scaling
            CGPoint newZoomingCenter = CGPointMake(_centerOfZoomingInWorld.x * zoomFactor /_previousWorldScale,
                                                   _centerOfZoomingInWorld.y * zoomFactor /_previousWorldScale);
            CGFloat deltaX = _centerOfZoomingInWorld.x - newZoomingCenter.x;
            CGFloat deltaY = _centerOfZoomingInWorld.y - newZoomingCenter.y;
            
            // Re-position the world
            CGPoint newWorldPosition = CGPointMake(_worldLocationBeforeZoom.x + deltaX, _worldLocationBeforeZoom.y + deltaY);
            [_world setPosition:newWorldPosition];
            
            // Set the previous center of zooming to the current one
            // This will allow dragging on the screen at the same time as zooming
//            CGPoint centerOfZoomingInView = SPVectorPointBetweenPoints(touch1NewLocation, touch2NewLocation);
//            CGFloat worldY = self.frame.size.height - centerOfZoomingInView.y; // Y axis is inverted in the world
//            _centerOfZoomingInWorld = CGPointMake((centerOfZoomingInView.x) - (_world.position.x/_world.scale),
//                                                  worldY - (_world.position.y/_world.scale));
            
            
            _isMoving = YES;
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    if (event.type == UIEventTypeTouches && touches.count == 1) {
        if (!_isMoving) {
            [_currentSelectedCell setIsSelected:NO];
            _currentSelectedCell = nil;
        }
        _isMoving = NO;
    }
    else if (event.type == UIEventTypeTouches && touches.count == 2){
        if (!_isMoving) {
            [_currentSelectedCell setIsSelected:NO];
            _currentSelectedCell = nil;
        }
        _isMoving = NO;
    }
}

#pragma mark - Game loop

#define BASE_FRAME_RATE  60.0f
#define MIN_FRAME_RATE   20.0f

-(void)update:(NSTimeInterval)currentTime{
    
    static BOOL endGameDetected;
    double delta = currentTime - _previousTime;
    
    if (_gameStatus == GameInitialized) {
        _gameTimer = 0.0;
        _gameStatus = GameRunning;
        endGameDetected = NO;
    }
    
    else if (_gameStatus == GameRunning) {
        
        _gameTimer += delta;
        
        // Update the superview's clock
        [_gameSceneDelegate gameScene:self gameClockUpdated:(int)ceilf(_gameTimer)];
    
        // Perform many updates in a row if the FPS value is lower than 60
        // This will allow spores to match their speed to the current FPS and deal with the lag.
        int roundsNb = floorf(delta*BASE_FRAME_RATE);
        roundsNb = roundsNb==0?1:(roundsNb>3)?3:roundsNb;
        CGFloat miniDelta = delta / (CGFloat)roundsNb;
        
        // If the FPS value is too low, skip the following update.
        // Give Sprite Kit some well deserved rest.
        if (delta>(1/MIN_FRAME_RATE)) roundsNb = 0;
        
        // Game over variables
        __block BOOL userHasCells = NO;
        BOOL userHasFlock = NO;
        __block BOOL userAttacks = NO;
        BOOL computerHasFlock = NO;
        __block BOOL computerAttacks = NO;
        NSMutableArray *computerOwnedCells = [[NSMutableArray alloc] init];
        NSMutableArray *otherCells = [[NSMutableArray alloc] init];
        
        while (roundsNb > 0) {
        
            
            // Move all flocks to their destinations and decay
            NSUInteger i = 0;
            while (i < _flocks.count) {
                SPFlock *flock = [_flocks objectAtIndex:i];
                
                // Compute new flocking vectors once per update call to save some CPU
                // Spores carry on with their previous velocity
                if ([flock update:miniDelta withDecay:_isDecayAllowed andFlocking:roundsNb==1?YES:NO]) {
                    if ([flock faction] == FactionPlayer) userHasFlock = YES;
                    else if ([flock faction] == FactionComputer) computerHasFlock = YES;
                    i++;
                }else{
                    [_flocks removeObject:flock];
                }
            }
            
            // Update all cells
            [_world enumerateChildNodesWithName:@"Cell" usingBlock:^(SKNode *node, BOOL *stop) {
                
                SPCell *cellNode = (SPCell*)node;
                
                // Update the cells logic and check if the cell is offscreen
                CGPoint cellScreenPosition = CGPointMake(cellNode.position.x+(_world.position.x*_world.scale),
                                                         cellNode.position.y+(_world.position.y*_world.scale));
                [cellNode update:miniDelta offScreen:!CGRectContainsPoint(self.frame, cellScreenPosition)];
                
                // Build sorted cells arrays for the future AI update, and set up the flags for win/lose detection
                if ([cellNode faction] == FactionComputer){
                    [computerOwnedCells addObject:cellNode];
                    for (SPSpore *spore in [cellNode incomingSpores]) {
                        if ([spore faction] == FactionPlayer){
                            userAttacks = YES;
                            break;
                        }
                    }
                }
                else if([cellNode faction] == FactionPlayer){
                    userHasCells = YES;
                    [otherCells addObject:cellNode];
                    for (SPSpore *spore in [cellNode incomingSpores]) {
                        if ([spore faction] == FactionComputer){
                            computerAttacks = YES;
                            break;
                        }
                    }
                }else{
                    [otherCells addObject:cellNode];
                    for (SPSpore *incomingSpore in [cellNode incomingSpores]) {
                        if (incomingSpore.faction == FactionComputer) computerAttacks = YES;
                        else userAttacks = YES;
                    }
                }
                
                // Check weither the cell is off-screen. If so, display its pointer on the edge of the screen
                //            if ([cellNode faction] != FactionNeutral) {
                //                CGPoint p = CGPointMake(_world.position.x + cellNode.position.x,
                //                                        _world.position.y + cellNode.position.y);
                //                // If the cell appears in the view
                //                if (CGRectContainsPoint(self.frame,p)) {
                //                    if ([cellNode offScreenPointer]) {
                //                        [[cellNode offScreenPointer] removeFromParent];
                //                        [cellNode setOffScreenPointer:nil];
                //                    }
                //                }
                //                // If it is off screen
                //                else{
                //                    if (![cellNode offScreenPointer]) {
                //                        [cellNode setOffScreenPointer:[SKSpriteNode spriteNodeWithImageNamed:@"OffScreenPointer"]];
                //                        [self addChild:[cellNode offScreenPointer]];
                //                    }
                //                    CGFloat x = cellNode.position.x;
                //                    CGFloat y = cellNode.position.y;
                //                    x = x>self.frame.size.width?self.frame.size.width-10:(x<0?10:x);
                //                    y = y>self.frame.size.height?self.frame.size.height-10:(y<0?10:y);
                //                }
                //            }
                
            }];
            
            // Update the AI
            [_dumbAI update:miniDelta
                   forCells:computerOwnedCells
             withOtherCells:otherCells];
         
            // Check win/lose conditions (starting 5 seconds after the beginning of the game)
            if (_gameTimer > 5.0 && !endGameDetected) {
                // Win condition
                if (computerOwnedCells.count == 0 && !computerHasFlock && !computerAttacks) {
                    endGameDetected = YES;
                    double delayInSeconds = 1.0;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationGameOver object:nil userInfo:@{@"outcome":@"playerWon"}];
                    });
                }
                
                // Lose condition
                else if (!userHasCells && !userHasFlock && !userAttacks){
                    endGameDetected = YES;
                    double delayInSeconds = 1.0;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationGameOver object:nil userInfo:@{@"outcome":@"computerWon"}];
                    });
                }
            }
            
            roundsNb --;
            
        }
        
        // Check for available events
        if (_events.count > 0) {
            SPRegisteredGameEvent *nextEvent = [_events objectAtIndex:0];
            if (nextEvent.time <= _gameTimer) {
                if (nextEvent.eventTriggered == NO && nextEvent.eventCompleted == NO) {
                    
                    // Event hasn't been trigered yet, launch it
                    [nextEvent setEventTriggered:YES];
                    
                    // Check the type of event
                    if (nextEvent.eventType == EventTypeCard) {
                        
                        // Send a notification
                        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationGameEvent object:nil userInfo:@{@"event": nextEvent}];
                    }
                    
                }
                else if(nextEvent.eventCompleted == YES){
                    // Even has been completed, remove it
                    [_events removeObjectAtIndex:0];
                }
            }
        }
        
    }
    _previousTime = currentTime;
}

//// Final step before rendering
//-(void)didSimulatePhysics{
//
//}

#pragma mark - Game Events methods

-(void)skipNextGameEvents{
    for (SPRegisteredGameEvent *nextEvent in _events) {
        [nextEvent setEventCompleted:YES];
    }
}

-(void)gameEventDismissed{
    if ([_events count] > 0) {
        SPRegisteredGameEvent *nextEvent = [_events objectAtIndex:0];
        [nextEvent setEventCompleted:YES];
    }
}

#pragma mark - SPAIDelegate methods

-(void)AI:(SPDumbAI *)AI performsActionFromCell:(SPCell *)cell toCell:(SPCell *)otherCell{
    SPFlock *flock = [[SPFlock alloc] initFromWorld:_world];
    [flock setOrigin:cell];
    [flock setDestination:otherCell];
    [flock setFaction:cell.faction];
    [flock setSpeed:4];
    [flock spawnSpores:[cell flockCreated]];
    [_flocks addObject:flock];
}

@end
