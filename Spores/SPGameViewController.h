//
//  SPGameViewController.h
//  Spores
//
//  Created by Julien Ducret on 15/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPGameViewController : UIViewController

@property (nonatomic, assign) int currentLevel;

- (void)pauseGame;

- (void)resumeGame;

-(void)buildSceneFromLevel:(NSUInteger)levelIndex;

@end
