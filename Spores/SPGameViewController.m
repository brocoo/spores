//
//  SPGameViewController.m
//  Spores
//
//  Created by Julien Ducret on 15/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPGameViewController.h"
#import <SpriteKit/SpriteKit.h>
#import "SPGameScene.h"
#import "SPLevelManager.h"
#import "SPGameNotificationManager.h"
#import "SPTopNotificationView.h"
#import "SPRegisteredGameEvent.h"
#import "SPEventView.h"
#import "GPUImage.h"
#import "SPWinMenu.h"
#import "SPLostMenu.h"
#import "SPLoadingView.h"
#import <Social/Social.h>
#import "SPFacebookManager.h"
#import "NSString+Utils.h"
#import "Flurry.h"
#import "SPSettingsViewController.h"
#import "SPGameKitManager.h"

@interface SPGameViewController ()<SPLevelManagerDelegate, SPGameNotificationManagerDelegate, SPGameSceneDelegate, SPGameOverMenuDelegate, SPEventViewDelegate>

@property (nonatomic, weak) IBOutlet SKView *gameView;
@property (nonatomic, strong) SPGameScene *gameScene;
@property (nonatomic, weak) IBOutlet UIView *topNotificationView;
@property (nonatomic, weak) IBOutlet UIImageView *blurredView;
@property (nonatomic, weak) IBOutlet UIView *menuView;
@property (nonatomic, weak) IBOutlet UIButton *restartButton;
@property (nonatomic, weak) IBOutlet UIButton *confirmRestartButton;
@property (nonatomic, weak) IBOutlet UIButton *cancelRestartButton;
@property (nonatomic, weak) IBOutlet UIButton *menuButton;
@property (nonatomic, weak) IBOutlet UIButton *confirmMenuButton;
@property (nonatomic, weak) IBOutlet UIButton *cancelMenuButton;
@property (nonatomic, weak) IBOutlet UILabel *gameOverLabel;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *pauseBarButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *settingsBarButton;
@property (nonatomic, strong) SPGameNotificationManager *notificationManager;
@property (nonatomic, assign) NSUInteger nbNotificationDisplayed;
@property (nonatomic, strong) NSMutableArray *notificationViews;
@property (nonatomic, weak) IBOutlet UILabel *timeElapsedLabel;
@property (nonatomic, assign) NSUInteger clock;
@property (nonatomic, weak) IBOutlet UIImageView *backgroundView;
@property (nonatomic, assign) BOOL isFirstAppereance;
@property (nonatomic, assign) BOOL settingsViewControllerDisplayed;

@end

@implementation SPGameViewController

#pragma mark - Initialization and life cycle methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _currentLevel = 1;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    if (!RELEASE_MODE) {
        _gameView.showsDrawCount = YES;
        _gameView.showsNodeCount = YES;
        _gameView.showsFPS = YES;
    }
    
    _settingsViewControllerDisplayed = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    if (!_settingsViewControllerDisplayed) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [super viewWillAppear:animated];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(gameEventReceived:)
                                                     name:kNotificationGameEvent
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(gameOver:)
                                                     name:kNotificationGameOver
                                                   object:nil];
        
        if (_gameView.scene == nil) [_gameView setAlpha:0.0];
        [self buildSceneFromLevel:_currentLevel];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (_settingsViewControllerDisplayed) {
        _settingsViewControllerDisplayed = NO;
        [_gameView.scene setPaused:NO];
        [(SPGameScene*)_gameView.scene setGameStatus:GameRunning];
    }
}

-(void)buildSceneFromLevel:(NSUInteger)levelIndex{
    
    _notificationManager = [[SPGameNotificationManager alloc] init];
    [_notificationManager setNotificationDelegate:self];
    _nbNotificationDisplayed = 0;
    _notificationViews = [[NSMutableArray alloc] init];
    
    // Display the loading view while building the level
    SPLoadingView *loadingView = [[[NSBundle mainBundle] loadNibNamed:@"SPLoadingView" owner:nil options:nil] firstObject];
    [self.view addSubview:loadingView];
    [loadingView showAnimated:YES withCompletion:^{
        SPLevelManager *lm = [SPLevelManager shared];
        [lm parseLevel:levelIndex withCompletion:^(NSDictionary *data) {
            NSDictionary *levelContent = data;
            // Hide the loading view after one seconde and display the level
            double delayInSeconds = 1.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                _gameScene = nil;
                _gameScene = [[SPGameScene alloc] initWithSize:CGSizeMake(_gameView.frame.size.width,
                                                                          _gameView.frame.size.height)];
                [_gameScene setGameSceneDelegate:self];
                [_gameScene setLevelContent:levelContent];
                [_gameView presentScene:_gameScene];
                [_gameView setAlpha:1.0];
                [_timeElapsedLabel setText:@"00:00"];
                [loadingView hideAnimated:YES withCompletion:^{
                    [loadingView removeFromSuperview];
                }];
            });
        }];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pauseGame{
    if (_gameScene && [_gameScene gameStatus] != GamePaused) [_gameView setPaused:YES];
}

- (void)resumeGame{
    if (_gameScene && [_gameScene gameStatus] != GamePaused) [_gameView setPaused:NO];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Top notifications handling
    
-(BOOL)notificationManager:(SPGameNotificationManager *)notificationManager displayTopNotificationMessage:(NSString *)message{
    
    SPTopNotificationView *newNotificationView = [[[NSBundle mainBundle] loadNibNamed:@"SPTopNotificationView" owner:self options:nil] objectAtIndex:0];
    [newNotificationView.title setText:message];
    [newNotificationView setTag:-1];
    [newNotificationView setAlpha:0.0];
    [newNotificationView setFrame:CGRectMake(0, -30, 320, 30)];
    [_topNotificationView addSubview:newNotificationView];
    
    [UIView animateWithDuration:0.2 animations:^{
        for (SPTopNotificationView *notificationView in _topNotificationView.subviews) {
            [notificationView setFrame:CGRectMake(0, (notificationView.tag+1)*30, 320, 30)];
            if (notificationView.tag == -1) [notificationView setAlpha:1.0];
            else if (notificationView.tag == 2) [notificationView setAlpha:0.0];
        }        
    } completion:^(BOOL finished) {
        for (SPTopNotificationView *notificationView in _topNotificationView.subviews) {
            notificationView.tag ++;
            if (notificationView.tag == 3){
                [notificationView removeFromSuperview];
                [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(lastTopNotificationExpired:) object:notificationView];
            }
        }
        [_notificationManager dequeueNotifications];
        
    }];
    
    [self performSelector:@selector(lastTopNotificationExpired:) withObject:newNotificationView afterDelay:5.0];
    return YES;
}

-(void)lastTopNotificationExpired:(SPTopNotificationView*)notificationView{
    [UIView animateWithDuration:0.2 animations:^{
        [notificationView setAlpha:0.0];
    } completion:^(BOOL finished) {
        [notificationView removeFromSuperview];
    }];
}

-(void)clearAllTopNotifications{
    for (SPTopNotificationView *notificationView in _topNotificationView.subviews) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(lastTopNotificationExpired:) object:notificationView];
        [self performSelector:@selector(lastTopNotificationExpired:) withObject:notificationView afterDelay:0.2];
    }
    [_notificationManager dequeueNotifications];
}

#pragma mark - IBAction methods

-(IBAction)confirmTapped:(UIButton*)sender{
    
    [self hideMenuAnimatedWithCompletion:^{
        if (sender == _confirmRestartButton) {
        
            [self buildSceneFromLevel:_currentLevel];
            
        }else if (sender == _confirmMenuButton) {
            
            [Flurry logEvent:kLevelWin withParameters:@{kLevelClock:[NSNumber numberWithInt:_clock],kLevelName:[NSString stringWithFormat:@"level%d",_currentLevel]}];
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    }];
}

-(IBAction)buttonTapped:(UIButton*)sender{
    
    UIButton *button;
    UIButton *confirmButton;
    UIButton *cancelButton;
    
    if (sender == _restartButton) {
        button = _restartButton;
        confirmButton = _confirmRestartButton;
        cancelButton = _cancelRestartButton;
        [self cancelTapped:_cancelMenuButton];
    }else if(sender == _menuButton){
        button = _menuButton;
        confirmButton = _confirmMenuButton;
        cancelButton = _cancelMenuButton;
        [self cancelTapped:_cancelRestartButton];
    }
    
    CGRect buttonFrame = button.frame;
    buttonFrame.origin.x = 4;
    CGRect confirmFrame = confirmButton.frame;
    confirmFrame.origin.x = 162;
    CGRect cancelFrame = cancelButton.frame;
    cancelFrame.origin.x = 4;
    
    [UIView animateWithDuration:0.15
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [button setFrame:buttonFrame];
                         [button setAlpha:0.0];
                         [confirmButton setFrame:confirmFrame];
                         [confirmButton setAlpha:1.0];
                         [cancelButton setFrame:cancelFrame];
                         [cancelButton setAlpha:1.0];
                     }
                     completion:^(BOOL finished) {}
     ];
}

-(IBAction)cancelTapped:(UIButton*)sender{
    
    UIButton *button;
    UIButton *confirmButton;
    UIButton *cancelButton;
    
    if (sender == _cancelRestartButton) {
        button = _restartButton;
        confirmButton = _confirmRestartButton;
        cancelButton = _cancelRestartButton;
    }else if(sender == _cancelMenuButton){
        button = _menuButton;
        confirmButton = _confirmMenuButton;
        cancelButton = _cancelMenuButton;
    }
    
    CGRect buttonFrame = button.frame;
    buttonFrame.origin.x = 4;
    CGRect confirmFrame = confirmButton.frame;
    confirmFrame.origin.x = 320+162;
    CGRect cancelFrame = cancelButton.frame;
    cancelFrame.origin.x = 320+4;

    [UIView animateWithDuration:0.15
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                             [button setFrame:buttonFrame];
                             [button setAlpha:1.0];
                             [confirmButton setFrame:confirmFrame];
                             [confirmButton setAlpha:0.0];
                             [cancelButton setFrame:cancelFrame];
                             [cancelButton setAlpha:0.0];
                     }
                     completion:^(BOOL finished) {}
     ];
}

-(IBAction)resumeGame:(UIButton*)sender{
    [self hideMenuAnimatedWithCompletion:^{
        [_settingsBarButton setEnabled:YES];
        [_gameView.scene setPaused:NO];
        [(SPGameScene*)_gameView.scene setGameStatus:GameRunning];
    }];
}

-(IBAction)pauseButtonTapped:(UIBarButtonItem*)sender{
    
    GameStatus gameStatus = [(SPGameScene*)_gameView.scene gameStatus];
    
    if (gameStatus == GamePaused) {
        
        [self hideMenuAnimatedWithCompletion:^{
            [_settingsBarButton setEnabled:YES];
            [_gameView.scene setPaused:NO];
            [(SPGameScene*)_gameView.scene setGameStatus:GameRunning];
        }];
        
    }else{
        
        // Render a blurred image of the scene
//        UIView *renderedScene = [self.view snapshotViewAfterScreenUpdates:YES];
//        
//        UIGraphicsBeginImageContextWithOptions(renderedScene.frame.size, NO, 0.0);
//        [renderedScene.layer renderInContext:UIGraphicsGetCurrentContext()];
//        UIImage *renderedSceneImage = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        
//        
//        GPUImageFastBlurFilter *blurFilter = [[GPUImageFastBlurFilter alloc] init];
//        blurFilter.blurSize = 100;
//        UIImage *blurredSceneImage = [blurFilter imageByFilteringImage:renderedSceneImage];
//        [_blurredView setImage:blurredSceneImage];
//        
//        UIImageWriteToSavedPhotosAlbum(blurredSceneImage, nil, nil, nil);

        [_gameView.scene setPaused:YES];
        [_settingsBarButton setEnabled:NO];
        [(SPGameScene*)_gameView.scene setGameStatus:GamePaused];
        [self showMenuAnimatedWithCompletion:nil];
    }
}

- (IBAction)settingsButtonTapped:(UIBarButtonItem*)sender{
    SPSettingsViewController *settingsVC = [[SPSettingsViewController alloc] init];
    _settingsViewControllerDisplayed = YES;
    [self presentViewController:settingsVC animated:YES completion:^{
        [_gameView.scene setPaused:YES];
        [(SPGameScene*)_gameView.scene setGameStatus:GamePaused];
    }];
}

#pragma mark - Menu handling

-(void)showMenuAnimatedWithCompletion:(CompletionBlock)completion{
    [UIView animateWithDuration:0.15
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [_blurredView setAlpha:1.0];
                         [_menuView setCenter:CGPointMake(self.view.center.x,
                                                          self.view.center.y)];
                     }
                     completion:^(BOOL finished) {
                         if (completion!=nil) completion();
                     }
     ];
}

-(void)hideMenuAnimatedWithCompletion:(CompletionBlock)completion{
    [self cancelTapped:_cancelRestartButton];
    [self cancelTapped:_cancelMenuButton];
    [UIView animateWithDuration:0.15
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [_blurredView setAlpha:0.0];
                         [_menuView setCenter:CGPointMake(self.view.center.x,
                                                          -_menuView.frame.size.height/2)];
                     }
                     completion:^(BOOL finished) {
                         if (completion!=nil) completion();
                     }
     ];
}

#pragma mark - Game Events handling

-(void)gameOver:(NSNotification*)notification{
    
    [_pauseBarButton setEnabled:NO];
    [_settingsBarButton setEnabled:NO];
    [_gameView.scene setPaused:YES];
    [(SPGameScene*)_gameView.scene setGameStatus:GamePaused];
    
    // Dismiss all scheduled and currently displayed event notifications and top notifications
    [(SPGameScene*)_gameView.scene skipNextGameEvents];
    [self clearAllTopNotifications];
    
    // It's a win
    if ([[notification.userInfo objectForKey:@"outcome"] isEqualToString:@"playerWon"]) {
        
        // Report score on GameCenter
        [[SPGameKitManager shared] reportTime:_clock forLevel:_currentLevel];
        
        // Check best time
        BOOL isBestScore = YES;
        if ([[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"level%d",_currentLevel]]) {
            NSUInteger bestScore = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"level%d",_currentLevel]];
            if (_clock < bestScore) {
                isBestScore = YES;
                [[NSUserDefaults standardUserDefaults] setInteger:_clock forKey:[NSString stringWithFormat:@"level%d",_currentLevel]];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }else{
                isBestScore = NO;
            }
        }else{
            [[NSUserDefaults standardUserDefaults] setInteger:_clock forKey:[NSString stringWithFormat:@"level%d",_currentLevel]];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        [Flurry logEvent:kLevelWin withParameters:@{kLevelClock:[NSNumber numberWithInt:_clock],kLevelName:[NSString stringWithFormat:@"level%d",_currentLevel]}];
        
        SPWinMenu *winMenu = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([SPWinMenu class]) owner:self options:nil] objectAtIndex:0];
        [[self view] addSubview:winMenu];
        [winMenu setBestScore:isBestScore];
        [winMenu setClock:_clock];
        [winMenu showAnimated:YES withCompletion:^{}];
        [winMenu setGameOverMenuDelegate:self];
        
    // It's a lose
    }else{
        
        [Flurry logEvent:kLevelLost withParameters:@{kLevelClock:[NSNumber numberWithInt:_clock],kLevelName:[NSString stringWithFormat:@"level%d",_currentLevel]}];
        
        SPLostMenu *lostMenu = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([SPLostMenu class]) owner:self options:nil] objectAtIndex:0];
        [[self view] addSubview:lostMenu];
        [lostMenu setGameOverMenuDelegate:self];
        [lostMenu showAnimated:YES withCompletion:^{}];
    }
}

-(void)gameEventReceived:(NSNotification*)gameEventNotification{
    
    [_pauseBarButton setEnabled:NO];
    [_settingsBarButton setEnabled:NO];
    
    [_gameView.scene setPaused:YES];
    [(SPGameScene*)_gameView.scene setGameStatus:GamePaused];
    
    SPRegisteredGameEvent *gameEvent = [[gameEventNotification userInfo] objectForKey:@"event"];
    
    // Build the view and display the event
    SPEventView *eventView = [[[NSBundle mainBundle] loadNibNamed:@"SPEventView" owner:self options:nil] firstObject];
    [eventView setEventViewDelegate:self];
    [self.view addSubview:eventView];
    
    // Populate the content of the view
    [eventView.subTitleLabel setFrameForText:[gameEvent.content objectForKey:@"subtitle"] withMaxHeight:eventView.subTitleLabel.frame.size.height];
    [eventView.subTitleLabel setText:[gameEvent.content objectForKey:@"subtitle"]];
    [eventView.imageView setImage:[UIImage imageNamed:[gameEvent.content objectForKey:@"image"]]];
    
    [eventView showAnimated:YES withCompletion:^{}];
}

#pragma mark - Custom delegates

#pragma mark - SPGameEventDelegate methods

-(void)eventViewSkipButtonTapped:(SPEventView *)eventView{
    [(SPGameScene*)_gameView.scene skipNextGameEvents];
    [self eventViewNextButtonTapped:eventView];
}

-(void)eventViewNextButtonTapped:(SPEventView *)eventView{
    [_pauseBarButton setEnabled:YES];
    [_settingsBarButton setEnabled:YES];
    [eventView hideAnimated:YES withCompletion:^{
        [eventView removeFromSuperview];
        [(SPGameScene*)_gameView.scene gameEventDismissed];
        [_gameView.scene setPaused:NO];
        [(SPGameScene*)_gameView.scene setGameStatus:GameRunning];
    }];
}

#pragma mark - SPGameSceneDelegate methods

-(void)gameScene:(SPGameScene *)gameScene gameClockUpdated:(NSUInteger)time{
    [_timeElapsedLabel setText:[NSString stringFromTimeInteger:time]];
    _clock = time;
}

#pragma mark - SPGameOverMenuDelegate methods

-(void)gameOverMenuBackTapped:(SPGameOverMenu *)menu{
    [menu hideAnimated:YES withCompletion:^{
        [menu removeFromSuperview];
        [self.navigationController popViewControllerAnimated:YES];        
    }];
}

-(void)gameOverMenuNextLevelTapped:(SPGameOverMenu *)menu{
    [menu hideAnimated:YES withCompletion:^{
        [menu removeFromSuperview];
        [_pauseBarButton setEnabled:YES];
        [_settingsBarButton setEnabled:YES];
        _currentLevel ++;
        SPLevelManager *lm = [SPLevelManager shared];
        if ([lm isNextLevelAvailable:_currentLevel]) {
            [self buildSceneFromLevel:_currentLevel];
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

-(void)gameOverMenuRestartLevelTapped:(SPGameOverMenu *)menu{
    [menu hideAnimated:YES withCompletion:^{
        [menu removeFromSuperview];
        [_pauseBarButton setEnabled:YES];
        [_settingsBarButton setEnabled:YES];
        
        [self buildSceneFromLevel:_currentLevel];
    }];
}


-(void)gameOverMenuFacebookTapped:(SPGameOverMenu *)menu isBestScore:(BOOL)isBestScore{
    [[SPFacebookManager shared] shareTime:_clock forLevel:_currentLevel fromViewController:self];
}

-(void)gameOverMenuTwitterTapped:(SPGameOverMenu *)menu isBestScore:(BOOL)isBestScore{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *cvc = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [cvc setInitialText:[NSString stringWithFormat:@"Just finished level %d in %@. Try to beat me! #Spores",_currentLevel,[NSString stringFromTimeInteger:_clock]]];
        [self presentViewController:cvc animated:YES completion:nil];
        
    }
}

@end
