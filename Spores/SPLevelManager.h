//
//  SPLevelParser.h
//  Spores
//
//  Created by Julien Ducret on 18/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SPLevelManager;

@protocol SPLevelManagerDelegate <NSObject>

@end

@interface SPLevelManager : NSObject

@property (nonatomic,weak) id<SPLevelManagerDelegate> delegate;

+ (id)shared;

-(void)parseLevel:(NSUInteger)levelIndex withCompletion:(CompletionDataBlock)completion;

-(BOOL)isNextLevelAvailable:(NSUInteger)levelIndex;

-(void)randomLevelWithSettings:(NSDictionary*)settings withCompletion:(void(^)(NSDictionary* content))completion;

@end
