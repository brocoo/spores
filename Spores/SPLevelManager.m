//
//  SPLevelParser.m
//  Spores
//
//  Created by Julien Ducret on 18/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPLevelManager.h"

@interface SPLevelManager()

@property (nonatomic, strong) dispatch_queue_t levelParsingQueue;

@end

@implementation SPLevelManager

@synthesize delegate = _delegate;
@synthesize levelParsingQueue = _levelParsingQueue;

+ (id)shared{
    static SPLevelManager *levelParser = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        levelParser = [[SPLevelManager alloc] init];
    });
    return levelParser;
}

-(id)init{
    
    self = [super init];
    if (self) {
        _levelParsingQueue = dispatch_queue_create("com.broco.spores.levelParsingQueue", NULL);
    }
    return self;
}

-(void)parseLevel:(NSUInteger)levelIndex withCompletion:(CompletionDataBlock)completion{
    dispatch_async(_levelParsingQueue, ^{
        // Open the file from the bundle
        NSString *filename = [NSString stringWithFormat:@"level%d",levelIndex];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:filename ofType:@"json"];
        NSData *data = [NSData dataWithContentsOfFile:filePath];
        if (data){
            // Parse data
            NSError *error;
            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            if (error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(nil);
                });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(dictionary);
                });
            }
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil);
            });
        }
    });
}

-(BOOL)isNextLevelAvailable:(NSUInteger)levelIndex{
    // Open the file from the bundle
    NSString *filename = [NSString stringWithFormat:@"level%d",levelIndex];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:filename ofType:@"json"];
    if (filePath) return YES;
    else return NO;
}

-(void)randomLevelWithSettings:(NSDictionary*)settings withCompletion:(void(^)(NSDictionary* content))completion{
    dispatch_async(_levelParsingQueue, ^{
        // Parse the settings and build a dictionnary based on them
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           @"randomLevel", kName,
                                           [NSNumber numberWithInt:0], kIndex,
                                           [NSNumber numberWithBool:YES], kAllowDecay,
                                           [NSDictionary dictionary], kWorld,
                                           [NSDictionary dictionary], kContent,
                                           [NSDictionary dictionary], kAI,
                                           [NSArray array], kEvents,
                                           nil];
        
        if ([[settings objectForKey:@"worldSize"] isEqualToString:@"small"]) {
            
        }
        
        
        NSUInteger numberOfCells = [[settings objectForKey:@"worldSize"] integerValue];
        NSMutableArray *cellArray = [[NSMutableArray alloc] init];
        for (NSUInteger i = 0; i<numberOfCells; i++) {
            NSMutableDictionary *cellData = [NSMutableDictionary dictionary];
            if (i==0) { // Create a cell for the user
                [cellData setObject:[NSNumber numberWithInt:1] forKey:kFaction];
            }else if(i==1){ // Create a cell for the computer
                [cellData setObject:[NSNumber numberWithInt:2] forKey:kFaction];
            }else{
                [cellData setObject:[NSNumber numberWithInt:0] forKey:kFaction];
            }
            
            
            
            [cellArray addObject:cellData];
        }
        [dictionary setObject:[NSDictionary dictionaryWithObject:cellArray forKey:kCells]  forKey:kContent];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completion([NSDictionary dictionaryWithDictionary:dictionary]);
        });
    });
}

@end
