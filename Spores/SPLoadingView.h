//
//  SPLoadingView.h
//  Spores
//
//  Created by Julien Ducret on 23/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPGamePopView.h"

@interface SPLoadingView : SPGamePopView

@property (nonatomic, weak) IBOutlet UILabel *loadingLabel;

@end
