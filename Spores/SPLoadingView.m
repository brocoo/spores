//
//  SPLoadingView.m
//  Spores
//
//  Created by Julien Ducret on 23/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPLoadingView.h"

@interface SPLoadingView ()

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, weak) IBOutlet UIView *loadingView;
@property (nonatomic, weak) IBOutlet UIView *backgroundView;

@end

@implementation SPLoadingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)showAnimated:(BOOL)animated withCompletion:(CompletionBlock)completion{
    if (animated) {
        [_activityIndicator setAlpha:0.0];
        [_backgroundView setAlpha:0.0];
        [_loadingLabel setAlpha:0.0];
        [_loadingView setFrame:({
            CGRect frame = CGRectMake(S_WIDTH/2 - 0.5, S_HEIGHT/2 - 0.5, 1, 1);
            frame;
        })];
        [UIView animateWithDuration:0.2 animations:^{
            [_backgroundView setAlpha:1.0];
            [_loadingView setFrame:({
                CGRect frame = CGRectMake(S_WIDTH/2 - 80, S_HEIGHT/2 - 80, 160, 160);
                frame;
            })];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1 animations:^{
                [_loadingView setFrame:({
                    CGRect frame = CGRectMake(S_WIDTH/2 - 60, S_HEIGHT/2 - 60, 120, 120);
                    frame;
                })];
            } completion:^(BOOL finished) {
                [_activityIndicator setAlpha:1.0];
                [_activityIndicator startAnimating];
                [_loadingLabel setAlpha:1.0];
                if (completion) completion();
            }];
        }];
    }else{
        [_loadingView setFrame:({
            CGRect frame = CGRectMake(S_WIDTH/2 - 60, S_HEIGHT/2 - 60, 120, 120);
            frame;
        })];
        [_backgroundView setAlpha:1.0];
        [_activityIndicator setAlpha:1.0];
        [_activityIndicator startAnimating];
        [_loadingLabel setAlpha:1.0];
        if (completion) completion();
    }
}

-(void)hideAnimated:(BOOL)animated withCompletion:(CompletionBlock)completion{
    if (animated) {
        [_activityIndicator setAlpha:0.0];
        [_activityIndicator stopAnimating];
        [_loadingLabel setAlpha:0.0];
        [UIView animateWithDuration:0.2 animations:^{
            [_loadingView setFrame:({
                CGRect frame = CGRectMake(S_WIDTH/2 - 80, S_HEIGHT/2 - 80, 160, 160);
                frame;
            })];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1 animations:^{
                [_backgroundView setAlpha:0.0];
                [_loadingView setFrame:({
                    CGRect frame = CGRectMake(S_WIDTH/2 - 0.5, S_HEIGHT/2 - 0.5, 1, 1);
                    frame;
                })];
            } completion:^(BOOL finished) {
                if (completion) completion();
            }];
        }];
    }else{
        [self setAlpha:1.0];
        [_loadingView setFrame:({
            CGRect frame = CGRectMake(S_WIDTH/2 - 60, S_HEIGHT/2 - 60, 120, 120);
            frame;
        })];
        [_backgroundView setAlpha:1.0];
        [_activityIndicator setAlpha:1.0];
        [_activityIndicator startAnimating];
        [_loadingLabel setAlpha:1.0];
        if (completion) completion();
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
