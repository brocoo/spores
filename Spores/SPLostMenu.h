//
//  SPLostMenu.h
//  Spores
//
//  Created by Julien Ducret on 19/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPGameOverMenu.h"

@interface SPLostMenu : SPGameOverMenu

@end
