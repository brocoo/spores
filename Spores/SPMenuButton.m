//
//  SPMenuButton.m
//  Spores
//
//  Created by Julien Ducret on 31/08/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPMenuButton.h"
#import <QuartzCore/QuartzCore.h>

@implementation SPMenuButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib{
    
    [self setBackgroundColor:[UIColor colorWithWhite:0.2 alpha:0.5]];
    [self.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:self.titleLabel.font.pointSize]];
    
//    [self.layer setBorderColor:[[UIColor colorWithWhite:1.0 alpha:0.8] CGColor]];
//    [self.layer setBorderWidth:1.0f];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
