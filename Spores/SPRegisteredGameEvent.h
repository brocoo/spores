//
//  SPRegisteredGameEvent.h
//  Spores
//
//  Created by Julien Ducret on 20/07/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SPRegisteredGameEvent;

enum EventType {
    EventTypeCamera = 0,
    EventTypeCard = 1
};
typedef enum EventType EventType;

@interface SPRegisteredGameEvent : NSObject

@property (nonatomic, readwrite, assign) BOOL eventTriggered;
@property (nonatomic, readonly, assign) CGFloat time;
@property (nonatomic, readonly, assign) EventType eventType;
@property (nonatomic, readwrite, assign) BOOL eventCompleted;
@property (nonatomic, readonly, strong) NSDictionary *content;

-(id)initWithContent:(NSDictionary*)content;

@end
