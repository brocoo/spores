//
//  SPRegisteredGameEvent.m
//  Spores
//
//  Created by Julien Ducret on 20/07/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPRegisteredGameEvent.h"

@implementation SPRegisteredGameEvent

@synthesize content = _content;
@synthesize eventTriggered = _eventTriggered;
@synthesize time = _time;
@synthesize eventType = _eventType;
@synthesize eventCompleted = _eventCompleted;

-(id)initWithContent:(NSDictionary*)content{
    self = [super init];
    if (self) {
        _content = content;
        _eventTriggered = NO;
        _eventCompleted = NO;
        _content = content;
        _time = [[content objectForKey:@"time"] floatValue];
        if ([[content objectForKey:@"type"] isEqualToString:@"card"]) _eventType = EventTypeCard;
    }
    return self;
}

@end
