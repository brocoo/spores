//
//  SPSettingsViewController.m
//  Spores
//
//  Created by Julien Ducret on 29/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPSettingsViewController.h"
#import "SPSoundManager.h"
#import <QuartzCore/QuartzCore.h>
#import "SPGameKitManager.h"

@interface SPSettingsViewController ()<UIScrollViewDelegate>

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UIScrollView *cardsScrollView;
@property (nonatomic, weak) IBOutlet UISwitch *musicSwitch;
@property (nonatomic, weak) IBOutlet UISwitch *topNotificationSwitch;
@property (nonatomic, weak) IBOutlet UISwitch *gameCenterSwitch;
@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;

@end

@implementation SPSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    
    if (IS_IPHONE5) [_scrollView setScrollEnabled:NO];
    else [_scrollView setScrollEnabled:YES];
    [_scrollView setContentSize:CGSizeMake(0, 568)];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kMusicEnabled]) {
        [_musicSwitch setOn:[[NSUserDefaults standardUserDefaults] boolForKey:kMusicEnabled]];
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kTopNotificationsEnabled]){
        [_topNotificationSwitch setOn:[[NSUserDefaults standardUserDefaults] boolForKey:kTopNotificationsEnabled]];
    }

    if ([[NSUserDefaults standardUserDefaults] objectForKey:kGameCenterShouldShare]){
        [_gameCenterSwitch setOn:[[NSUserDefaults standardUserDefaults] boolForKey:kGameCenterShouldShare]];
    }
    
    for (NSInteger i=0; i<6; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"tuto%d",i+1]]];
        [imageView setFrame:CGRectMake(i*225, 0, 225, 200)];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        [_cardsScrollView addSubview:imageView];
    }
    [_cardsScrollView setContentSize:CGSizeMake(6*225, 0)];
    [_pageControl setNumberOfPages:6];
    [_pageControl setCurrentPage:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction methods

- (IBAction)closeButtonTapped:(UIButton*)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)musicSwitchTapped:(UISwitch*)sender{
    [[SPSoundManager shared] setMusicEnabled:[sender isOn]];
}

- (IBAction)topNotificationsSwitchTapped:(UISwitch*)sender{
    [[NSUserDefaults standardUserDefaults] setBool:[_topNotificationSwitch isOn] forKey:kTopNotificationsEnabled];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)gameCenterSwitchTapped:(UISwitch*)sender{
    [[SPGameKitManager shared] authenticateLocalPlayerFromViewController:self
                                                             showLoginUI:YES
                                                              withSucces:^(id data, BOOL success) {
                                                                  [[NSUserDefaults standardUserDefaults] setBool:[_gameCenterSwitch isOn] forKey:kGameCenterShouldShare];
                                                                  [[NSUserDefaults standardUserDefaults] synchronize];
                                                              }
                                                              andFailure:^(NSError *error) {
                                                                  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                                  [alertView show];
                                                              }
     ];
}

#pragma mark - UIScrollViewDelegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSInteger index = (int)(scrollView.contentOffset.x+_cardsScrollView.frame.size.width/2) / 225;
    [_pageControl setCurrentPage:index];
}

@end
