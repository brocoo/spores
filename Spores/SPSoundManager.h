//
//  SPSoundManager.h
//  Spores
//
//  Created by Julien Ducret on 18/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SPCell;

@interface SPSoundManager : NSObject

@property (nonatomic, assign) BOOL musicEnabled;

+ (id)shared;

- (void)setup;

- (void)playSoundtrack;

- (void)playSporeAttackSoundAtVolume:(CGFloat)volume;

- (void)playButtonSound;

- (void)playSporeAttackSoundForCell:(SPCell*)cell;

@end
