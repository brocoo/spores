//
//  SPSoundManager.m
//  Spores
//
//  Created by Julien Ducret on 18/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPSoundManager.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import <SpriteKit/SpriteKit.h>
#import "SPCell.h"

NSString * const Soundtrack = @"soundtrack_01";

@interface SPSoundManager ()

@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic, assign) NSTimeInterval currentSoundtrackTime;
@property (nonatomic, strong) NSArray *sporeSoundsIDsArray;
@property (nonatomic, assign) NSInteger currentSoundIndex;
@property (nonatomic, assign) SystemSoundID buttonSoundID;

@end

@implementation SPSoundManager

@synthesize audioPlayer = _audioPlayer;
@synthesize musicEnabled = _musicEnabled;
@synthesize currentSoundtrackTime = _currentSoundtrackTime;
@synthesize sporeSoundsIDsArray = _sporeSoundsIDsArray;
@synthesize currentSoundIndex = _currentSoundIndex;
@synthesize buttonSoundID = _buttonSoundID;

+ (id)shared{
    static SPSoundManager *musicManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        musicManager = [[SPSoundManager alloc] init];
    });
    return musicManager;
}

- (void)setup{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kMusicEnabled]) {
        _musicEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:kMusicEnabled];
    }else [self setMusicEnabled:YES];
    [self initSoundtrack];
    [self initSounds];
}

#pragma mark - Sounds handling

- (void)initSounds{
    NSMutableArray *array = [NSMutableArray array];
    for (NSUInteger i=0; i<10; i++) {
        NSString *audioFilePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"blop"] ofType:@"wav"];
        NSData *audioFileData = [NSData dataWithContentsOfFile:audioFilePath];
        NSError *error;
        AVAudioPlayer *ap = [[AVAudioPlayer alloc] initWithData:audioFileData error:&error];
        if (error) NSLog(@"%@",error.localizedDescription);
        [ap prepareToPlay];
        [ap setNumberOfLoops:0];
        [array addObject:ap];
    }
    _sporeSoundsIDsArray = [NSArray arrayWithArray:array];
    _currentSoundIndex = 0;
    
    CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:[[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"zipclick"] ofType:@"wav"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    AudioServicesCreateSystemSoundID(url, &_buttonSoundID);
}

- (void)playSporeAttackSoundAtVolume:(CGFloat)volume{
    NSInteger index = _currentSoundIndex%[_sporeSoundsIDsArray count];
    AVAudioPlayer *ap = [_sporeSoundsIDsArray objectAtIndex:index];
    [ap setVolume:volume];
    [ap play];
    _currentSoundIndex++;
}

- (void)playSporeAttackSoundForCell:(SPCell*)cell{
    if(_musicEnabled){
        SKAction *action = [SKAction playSoundFileNamed:@"blop.wav" waitForCompletion:NO];
        [cell runAction:action];
    }
}

- (void)playButtonSound{
    if (_musicEnabled) {
        AudioServicesPlaySystemSound(_buttonSoundID);
    }
}

#pragma mark - Soundtrack handling

- (void)initSoundtrack{
    // Start this awesome background music composed by Alex Moreton.
    NSString *audioFilePath = [[NSBundle mainBundle] pathForResource:Soundtrack ofType:@"mp3"];
    NSData *audioFileData = [NSData dataWithContentsOfFile:audioFilePath];
    NSError *error;
    _audioPlayer = [[AVAudioPlayer alloc] initWithData:audioFileData error:&error];
    if (error) NSLog(@"%@",error.localizedDescription);
    [_audioPlayer prepareToPlay];
    [_audioPlayer setNumberOfLoops:-1];
    _currentSoundtrackTime = 0;
}

- (void)playSoundtrack{
    if (_audioPlayer) [_audioPlayer play];
}

#pragma mark - Custom setter

- (void)setMusicEnabled:(BOOL)musicEnabled{
    [[NSUserDefaults standardUserDefaults] setBool:musicEnabled forKey:kMusicEnabled];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if (musicEnabled) [_audioPlayer play];
    else{
        _currentSoundtrackTime = [_audioPlayer currentTime];
        [_audioPlayer stop];
    }
    _musicEnabled = musicEnabled;
}

@end
