//
//  SPSpore.h
//  Spores
//
//  Created by Julien Ducret on 18/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
@class SPCell;

@interface SPSpore : SKSpriteNode

@property (nonatomic, readwrite, assign) SPVector velocity;
@property (nonatomic, readwrite, assign) CGPoint futurePosition;
@property (nonatomic, readwrite, assign) Faction faction;
@property (nonatomic, readwrite, assign) NSInteger weight;
@property (nonatomic, readwrite, assign) CGFloat lifespan;
@property (nonatomic, readwrite, assign) CGFloat age;
@property (nonatomic, readwrite, weak) SPCell *targetedCell;

@end
