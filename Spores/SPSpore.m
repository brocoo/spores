//
//  SPSpore.m
//  Spores
//
//  Created by Julien Ducret on 18/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPSpore.h"
#import "SPCell.h"

@implementation SPSpore

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if (_targetedCell) {
        [_targetedCell touchesBegan:touches withEvent:event];
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if (_targetedCell) {
        [_targetedCell touchesEnded:touches withEvent:event];
    }
}

@end
