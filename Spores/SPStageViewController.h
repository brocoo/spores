//
//  SPStageViewController.h
//  Spores
//
//  Created by Julien Ducret on 25/07/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPStageViewController : UIViewController

- (void)resumeViewController;

- (void)pauseViewController;

@end
