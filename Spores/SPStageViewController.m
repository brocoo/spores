//
//  SPStageViewController.m
//  Spores
//
//  Created by Julien Ducret on 25/07/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPStageViewController.h"
#import "SPGameViewController.h"
#import "SPSoundManager.h"
#import "NSString+Utils.h"
#import "SPSettingsViewController.h"
#import "SPGameKitManager.h"
#import <GameKit/GameKit.h>

@interface SPStageViewController () <GKGameCenterControllerDelegate>

@property (nonatomic, weak) IBOutlet UIImageView *parallaxBackground;
@property (nonatomic, weak) IBOutlet UIView *mainMenuView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIView *stageMenuView;
@property (nonatomic, weak) IBOutlet UIButton *gameButton;
@property (nonatomic, weak) IBOutlet UIButton *aboutButton;
@property (nonatomic, weak) IBOutlet UILabel *aboutLabel;
@property (nonatomic, weak) IBOutlet UIImageView *splashScreen;
@property (nonatomic, strong) NSMutableArray *levelButtonsArray;
@property (nonatomic, strong) SPGameViewController *gameViewController;
@property (nonatomic, assign) BOOL displaySplashScreen;
@property (nonatomic, assign) BOOL showFadeIn;

@end

@implementation SPStageViewController

#pragma mark - Initialization methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    // Check if it's the first time the user plays
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"firstLaunch"] == nil) {
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:NO]  forKey:@"firstLaunch"];
    }
    
    // Splash screen
    _displaySplashScreen = YES;
    _showFadeIn = YES;
    
    // Start this awesome background music composed by Alex Moreton.
    [[SPSoundManager shared] playSoundtrack];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:kMusicEnabled]) {
        [[SPSoundManager shared] setMusicEnabled:[[NSUserDefaults standardUserDefaults] boolForKey:kMusicEnabled]];
    }else{
        [[SPSoundManager shared] setMusicEnabled:YES];
    }
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:kTopNotificationsEnabled]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kTopNotificationsEnabled];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(_displaySplashScreen){
        [_mainMenuView setAlpha:0.0];
        [_stageMenuView setAlpha:0.0];
        [_aboutLabel setAlpha:0.0];
    }
    
    // Re-build the level buttons
    _levelButtonsArray = [NSMutableArray arrayWithCapacity:20];
    for (NSUInteger i = 0; i<20; i++) {
        int row = i / 4;
        int column = i - row*4;
        UIButton *levelButton = [[UIButton alloc] initWithFrame:CGRectMake(20 + column*70, 100 + row*70, 69, 69)];
        [_levelButtonsArray addObject:levelButton];
        [_stageMenuView addSubview:levelButton];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"level%d",i+1]]) {
            NSInteger clock = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"level%d",i+1]];
            UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(3, 54, 63, 15)];
            [timeLabel setFont:[UIFont systemFontOfSize:14]];
            [timeLabel setText:[NSString stringFromTimeInteger:clock]];
            [timeLabel setTextAlignment:NSTextAlignmentCenter];
            [timeLabel setTextColor:[UIColor colorWithWhite:1.0 alpha:0.4]];
            [levelButton addSubview:timeLabel];
            [levelButton setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.15]];
            [levelButton setTitleColor:[UIColor colorWithWhite:1.0 alpha:1.0] forState:UIControlStateNormal];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"StopWatch"]];
            [imageView setFrame:CGRectMake(2, 55, 12, 12)];
            [imageView setAlpha:0.4];
            [levelButton addSubview:imageView];
        }else{
            [levelButton setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.12]];
            [levelButton setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateNormal];
        }
        
        [levelButton setTitle:[NSString stringWithFormat:@"%d",i+1] forState:UIControlStateNormal];
        [[levelButton titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:40]];
        [levelButton setTag:i+1];
        [levelButton setShowsTouchWhenHighlighted:YES];
        if (i>3) {
            [levelButton setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.05]];
            [levelButton setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateNormal];
        }else{
            [levelButton addTarget:self action:@selector(levelButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated{
    
    if (_displaySplashScreen) {
        _displaySplashScreen = NO;
        double delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [UIView animateWithDuration:0.1 animations:^{
                [_splashScreen setFrame:({
                    CGRect frame = _splashScreen.frame;
                    frame.size = CGSizeMake(230, 230);
                    frame.origin = CGPointMake((S_WIDTH-230)/2, (S_HEIGHT-230)/2);
                    frame;
                })];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:2.0 animations:^{
                    [_mainMenuView setAlpha:1.0];
                    [_stageMenuView setAlpha:1.0];
                }];
                [UIView animateWithDuration:0.2 animations:^{
                    [_splashScreen setFrame:({
                        CGRect frame = _splashScreen.frame;
                        frame.size = CGSizeMake(1, 1);
                        frame.origin = CGPointMake((S_WIDTH-1)/2, (S_HEIGHT-1)/2);
                        frame;
                    })];
                } completion:^(BOOL finished) {
                    [_splashScreen setAlpha:0.0f];
                }];
            }];
        });
        
    }else{
        [UIView animateWithDuration:1.0 animations:^{
            [_mainMenuView setAlpha:1.0];
            [_stageMenuView setAlpha:1.0];
        }];
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    for (UIButton *levelButton in _levelButtonsArray) [levelButton removeFromSuperview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pauseViewController{
    if (_gameViewController) [_gameViewController pauseGame];
}

- (void)resumeViewController{
    if (_gameViewController) [_gameViewController resumeGame];
}

#pragma mark - UIButton action methods

- (IBAction)newGameButtonTapped:(UIButton*)sender{
    CGRect mainMenuFrame = _mainMenuView.frame;
    mainMenuFrame.origin.x = -mainMenuFrame.size.width;
    CGRect stageMenuFrame = _stageMenuView.frame;
    stageMenuFrame.origin.x = 0;
    CGRect parallaxBackgroundFrame = _parallaxBackground.frame;
    parallaxBackgroundFrame.origin.x = -parallaxBackgroundFrame.size.width + self.view.bounds.size.width;
    [UIView animateWithDuration:0.3 animations:^{
        [_mainMenuView setAlpha:0.0];
        [_mainMenuView setFrame:mainMenuFrame];
        [_aboutLabel setAlpha:0.0];
        [_stageMenuView setAlpha:1.0];
        [_stageMenuView setFrame:stageMenuFrame];
        [_parallaxBackground setFrame:parallaxBackgroundFrame];
    }];
}

-(IBAction)aboutButtonTapped:(UIButton*)sender{
    
    [_aboutLabel setAlpha:0.0];
    [UIView animateWithDuration:0.2 animations:^{
        [_aboutLabel setAlpha:1.0];
    }];
}

-(IBAction)levelButtonTapped:(UIButton*)sender{
    _gameViewController = [[SPGameViewController alloc] init];
    [_gameViewController setCurrentLevel:sender.tag];
    [self.navigationController pushViewController:_gameViewController animated:YES];
    [UIView animateWithDuration:0.2 animations:^{
        [_mainMenuView setAlpha:0.0];
        [_stageMenuView setAlpha:0.0];
    } completion:^(BOOL finished) {
    }];
}

-(IBAction)backButtonSelected:(UIButton*)sender{
    CGRect mainMenuFrame = _mainMenuView.frame;
    mainMenuFrame.origin.x = 0;
    CGRect stageMenuFrame = _stageMenuView.frame;
    stageMenuFrame.origin.x = 320;
    CGRect parallaxBackgroundFrame = _parallaxBackground.frame;
    parallaxBackgroundFrame.origin.x = 0;
    [UIView animateWithDuration:0.3 animations:^{
        [_mainMenuView setAlpha:1.0];
        [_mainMenuView setFrame:mainMenuFrame];
        [_stageMenuView setAlpha:0.0];
        [_stageMenuView setFrame:stageMenuFrame];
        [_parallaxBackground setFrame:parallaxBackgroundFrame];
    }];
}

- (IBAction)settingsButtonTapped:(UIButton*)sender{
    SPSettingsViewController *settingsVC = [[SPSettingsViewController alloc] init];
    [self presentViewController:settingsVC animated:YES completion:^{
        
    }];
}

- (IBAction)gameCenterButtonTapped:(UIButton*)sender{
    
    // Log the user in first
    [[SPGameKitManager shared] authenticateLocalPlayerFromViewController:self showLoginUI:YES withSucces:^(id data, BOOL success) {
        GKGameCenterViewController *gameCenter = [[GKGameCenterViewController alloc] init];
        [gameCenter setGameCenterDelegate:self];
        [self presentViewController:gameCenter animated:YES completion:^{
            
        }];
    } andFailure:^(NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }];
}

#pragma mark - GKGameCenterControllerDelegate

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController{
    [gameCenterViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end
