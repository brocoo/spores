//
//  SPTextureManager.h
//  Spores
//
//  Created by Julien Ducret on 11/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

@interface SPTextureManager : NSObject

@property (nonatomic, strong, readonly) SKTexture *sporeTexture;

+ (id)sharedManager;

@end
