//
//  SPTextureManager.m
//  Spores
//
//  Created by Julien Ducret on 11/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPTextureManager.h"
#import <SpriteKit/SpriteKit.h>

@implementation SPTextureManager

@synthesize sporeTexture = _sporeTexture;

+ (id)sharedManager
{
    static id sharedManager;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (SKTexture*)sporeTexture{
    
    if (!_sporeTexture) {
        static dispatch_once_t once;
        dispatch_once(&once, ^{
            _sporeTexture = [SKTexture textureWithImageNamed:@"Spore"];
        });
    }
    return _sporeTexture;
}

@end
