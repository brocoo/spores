//
//  SPTopNotificationView.h
//  Spores
//
//  Created by Julien Ducret on 11/07/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPTopNotificationView : UIView

@property (nonatomic, assign) IBOutlet UILabel *title;

@end
