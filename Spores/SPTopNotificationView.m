//
//  SPTopNotificationView.m
//  Spores
//
//  Created by Julien Ducret on 11/07/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPTopNotificationView.h"

@implementation SPTopNotificationView

@synthesize title = _title;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
