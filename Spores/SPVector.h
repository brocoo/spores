//
//  SPVector.h
//  Spores
//
//  Created by Julien Ducret on 21/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#ifndef Spores_SPVector_h
#define Spores_SPVector_h

struct SPVector {
    CGFloat x;
    CGFloat y;
};
typedef struct SPVector SPVector;

// Creates a vector from two coordinates
static inline SPVector SPVectorMake(CGFloat x, CGFloat y)
{
    SPVector v;
    v.x = x;
    v.y = y;
    return v;
}

// Creates a vector between points
static inline SPVector SPVectorBetweenPoints(CGPoint a, CGPoint b)
{
    SPVector v;
    v.x = b.x - a.x;
    v.y = b.y - a.y;
    return v;
}

// Length of a vector
static inline double SPVectorLength(SPVector v)
{
    return sqrt(pow(v.x,2)+pow(v.y,2));
}

// Creates a vector initialized to 0
static inline SPVector SPVectorMakeZero(){
    return SPVectorMake(0,0);
}

// Distance between two points
static inline double distanceBetweenPoints(CGPoint a, CGPoint b){
    return sqrt(pow(b.x-a.x,2)+pow(b.y-a.y,2));
}

// Divides a vector by a scalar
static inline SPVector SPVectorDivideByScalar(SPVector v, CGFloat s){
    return SPVectorMake(v.x/s, v.y/s);
}

// Returns the point located between two points
static inline CGPoint SPVectorPointBetweenPoints(CGPoint a, CGPoint b){
    return CGPointMake((a.x+b.x)/2, (a.y+b.y)/2);
}

#endif
