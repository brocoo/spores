//
//  SPWinMenu.h
//  Spores
//
//  Created by Julien Ducret on 18/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPGameOverMenu.h"

@interface SPWinMenu : SPGameOverMenu

@property (nonatomic, assign) NSUInteger clock;
@property (nonatomic, assign) BOOL bestScore;

@end
