//
//  SPWinMenu.m
//  Spores
//
//  Created by Julien Ducret on 18/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPWinMenu.h"

@interface SPWinMenu()

@property (nonatomic, weak) IBOutlet UILabel *timeTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@property (nonatomic, weak) IBOutlet UIView *timeView;
@property (nonatomic, weak) IBOutlet UILabel *winLabel;

@end

@implementation SPWinMenu

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)showAnimated:(BOOL)animated withCompletion:(CompletionBlock)completion{

    [super showAnimated:animated withCompletion:^(void){
        if (completion) completion();
        
        NSString *minutesString = [NSString stringWithFormat:_clock/60>9?@"%d":@"0%d",_clock/60];
        NSString *secondsString = [NSString stringWithFormat:_clock%60>9?@"%d":@"0%d",_clock%60];
        NSString *clockString = [NSString stringWithFormat:@"%@:%@", minutesString, secondsString];
        [_timeLabel setText:clockString];
        
        if (_bestScore) [_timeTitleLabel setText:@"New personnal record :"];
        else [_timeTitleLabel setText:@"Your time :"];
        
        // Show the time view
        [UIView animateWithDuration:0.2 delay:1.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            [_winLabel setCenter:CGPointMake(-160, _winLabel.center.y)];
            [_timeView setCenter:CGPointMake(160, _timeView.center.y)];
        } completion:^(BOOL finished) {
            
        }];
    }];
}

-(IBAction)facebookButtonTapped:(id)sender{
    [[super gameOverMenuDelegate] gameOverMenuFacebookTapped:self isBestScore:_bestScore];
}

-(IBAction)twitterButtonTapped:(id)sender{
    [[super gameOverMenuDelegate] gameOverMenuTwitterTapped:self isBestScore:_bestScore];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
