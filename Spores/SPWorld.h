//
//  SPWorld.h
//  Spores
//
//  Created by Julien Ducret on 18/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface SPWorld : SKNode

@property (nonatomic, readwrite, strong) SKNode *camera;
@property (nonatomic, readwrite, assign) CGSize size;
@property (nonatomic, readonly, assign) CGFloat scale;

-(void)createWorld;

-(void)moveCameraToPoint:(CGPoint)point duration:(CGFloat)duration;

-(void)zoomCamera:(CGFloat)zoom duration:(CGFloat)duration;

@end
