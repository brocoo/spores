//
//  SPWorld.m
//  Spores
//
//  Created by Julien Ducret on 18/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "SPWorld.h"

@implementation SPWorld

@synthesize camera = _camera;
@synthesize size = _size;
@synthesize scale = _scale;

-(void)createWorld{
    // Create the camera
    _camera = [SKNode node];
    [_camera setName:@"camera"];
    [self addChild:_camera];
    _scale = 1.0;
}

-(void)setScale:(CGFloat)scale{
    _scale = scale;
    [super setScale:scale];
}

#pragma mark - Camera handling

-(void)moveCameraToPoint:(CGPoint)point duration:(CGFloat)duration{
    SKAction *action = [SKAction moveTo:point duration:duration];
    [self runAction:action];
}

-(void)zoomCamera:(CGFloat)zoom duration:(CGFloat)duration{
    SKAction *action = [SKAction scaleBy:zoom duration:duration];
    [self runAction:action];
}

@end
