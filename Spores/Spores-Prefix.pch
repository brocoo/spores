//
//  Prefix header
//
//  The contents of this file are implicitly included at the beginning of every source file.
//

#import <Availability.h>

#ifndef __IPHONE_3_0
#warning "This project uses features only available in iOS SDK 3.0 and later."
#endif

#ifdef __OBJC__
    #import <UIKit/UIKit.h>
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
    #import "UIColor+Spores.h"
    #import "UILabel+Utils.h"
    #import "SPVector.h"

    #define RELEASE_MODE    1
    #define MUSIC_ENABLED   1

    // Screen sizes
    #define S_WIDTH ( [ [ UIScreen mainScreen ] bounds ].size.width)
    #define S_HEIGHT ( [ [ UIScreen mainScreen ] bounds ].size.height)

    // Device detection
    #define IS_IPHONE5 (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height==568)

    #define MAXIMUM_FLOCK_SIZE  30

    // NSUserDefaults keys
    #define kMusicEnabled               @"kMusicEnabled"
    #define kTopNotificationsEnabled    @"kTopNotificationsEnabled"
    #define kGameCenterAuthenticated    @"kGameCenterAuthenticated"
    #define kGameCenterShouldShare      @"kGameCenterShouldShare"

    // NSNotifications names
    #define kNotificationCellCaptured   @"kNotificationCellCaptured"
    #define kNotificationGameEvent      @"kNotificationGameEvent"
    #define kNotificationGameOver       @"kNotificationGameOver"

    // Flurry events
    #define kLevelWin                   @"Level Win"
    #define kLevelLost                  @"Level Lost"
    #define kLevelGaveUp                @"Level GaveUp"
    #define kLevelClock                 @"Level Time"
    #define kLevelName                  @"Level Name"

    // Level parsing keys
    #define kName                       @"name"
    #define kIndex                      @"index"
    #define kAllowDecay                 @"allowDecay"
    #define kWorld                      @"world"
    #define kSize                       @"size"
    #define kWidth                      @"width"
    #define kHeight                     @"height"
    #define kCamera                     @"camera"
    #define kX                          @"x"
    #define kY                          @"y"
    #define kContent                    @"content"
    #define kCells                      @"cells"
    #define kRadius                     @"radius"
    #define kFaction                    @"faction"
    #define kSporeCount                 @"sporeCount"
    #define kMaxSporeCount              @"maxSporeCount"
    #define kSpawnDelay                 @"spawnDelay"
    #define kAI                         @"AI"
    #define kInitialReactionDelay       @"initialReactionDelay"
    #define kReactionDelay              @"reactionDelay"
    #define kIsAggressive               @"isAggressive"
    #define kEvents                     @"events"
    #define kType                       @"type"
    #define kTitle                      @"title"
    #define kSubtitle                   @"subtitle"
    #define kButtonLabel                @"buttonLabel"
    #define kImage                      @"image"
    #define kTime                       @"time"
    #define kInteractionEnabled         @"interactionEnabled"


typedef enum Faction:NSUInteger {
    FactionNeutral = 0,
    FactionPlayer = 1,
    FactionComputer = 2
} Faction;

typedef void(^CompletionBlock)(void);
typedef void(^CompletionDataBlock)(id data);
typedef void(^CompletionSuccessBlock)(id data, BOOL success);
typedef void(^CompletionFailureBlock)(NSError *error);

#endif
