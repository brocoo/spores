//
//  UIButton+Utils.h
//  Spores
//
//  Created by Julien Ducret on 20/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Utils)

@end
