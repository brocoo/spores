//
//  UIButton+Utils.m
//  Spores
//
//  Created by Julien Ducret on 20/09/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "UIButton+Utils.h"
#import "SPSoundManager.h"

@implementation UIButton (Utils)

-(void)sendAction:(SEL)action to:(id)target forEvent:(UIEvent *)event{
    [super sendAction:action to:target forEvent:event];
    [[SPSoundManager shared] playButtonSound];
}

@end
