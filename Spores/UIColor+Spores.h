//
//  UIColor+Spores.h
//  Spores
//
//  Created by Julien Ducret on 18/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Spores)

+(UIColor*) sporeLightBlue;

+(UIColor*) sporeCellBlue;

+(UIColor*) sporeCellRed;

@end
