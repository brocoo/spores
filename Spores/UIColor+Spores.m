//
//  UIColor+Spores.m
//  Spores
//
//  Created by Julien Ducret on 18/06/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "UIColor+Spores.h"

@implementation UIColor (Spores)

+(UIColor*) sporeLightBlue{
    return [UIColor colorWithRed:26.0/255.0 green:169.0/255.0 blue:211.0/255.0 alpha:1.0];
}

+(UIColor*) sporeCellBlue{
    return [UIColor colorWithRed:67.0/255.0 green:117.0/255.0 blue:255.0/255.0 alpha:1.0];
}

+(UIColor*) sporeCellRed{
    return [UIColor colorWithRed:227.0/255.0 green:47.0/255.0 blue:73.0/255.0 alpha:1.0];
}

@end
