//
//  UILabel+Utils.h
//  Spores
//
//  Created by Julien Ducret on 21/07/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Utils)

-(void)setFrameForText:(NSString*)text withMaxHeight:(CGFloat)maxHeight;

-(void)setFrameForText:(NSString*)text;

+(CGFloat)heightForText:(NSString*)text labelWidth:(CGFloat)width labelMaxHeight:(CGFloat)maxHeight labelFont:(UIFont*)font lineBreakMode:(NSLineBreakMode)breakLineMode;

@end
