//
//  UILabel+Utils.m
//  Spores
//
//  Created by Julien Ducret on 21/07/13.
//  Copyright (c) 2013 Julien Ducret. All rights reserved.
//

#import "UILabel+Utils.h"

@implementation UILabel (Utils)

-(void)setFrameForText:(NSString*)text withMaxHeight:(CGFloat)maxHeight{
    CGFloat height = [UILabel heightForText:text
                                 labelWidth:self.frame.size.width
                             labelMaxHeight:maxHeight
                                  labelFont:self.font
                              lineBreakMode:[self lineBreakMode]];
    CGRect frame = self.frame;
    frame.size.height = height;
    [self setFrame:frame];
}

-(void)setFrameForText:(NSString*)text{
    CGFloat height = [UILabel heightForText:text labelWidth:self.frame.size.width labelMaxHeight:FLT_MAX labelFont:self.font lineBreakMode:self.lineBreakMode];
    CGRect frame = self.frame;
    frame.size.height = height;
    [self setFrame:frame];
}

+(CGFloat)heightForText:(NSString*)text labelWidth:(CGFloat)width labelMaxHeight:(CGFloat)maxHeight labelFont:(UIFont*)font lineBreakMode:(NSLineBreakMode)breakLineMode{
    CGSize maximumLabelSize = CGSizeMake(width, FLT_MAX);
    CGSize expectedLabelSize = [text sizeWithFont:font constrainedToSize:maximumLabelSize lineBreakMode:breakLineMode];
    return expectedLabelSize.height;
}

- (void)setFontSize:(CGFloat)size{
    [self setFont:[UIFont fontWithName:self.font.fontName size:size]];
}

@end


